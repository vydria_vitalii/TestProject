﻿#pragma once
#include "View.h"
#include "FilmBean.h"

class InfoView : virtual public View
{
public:
	InfoView() = default;
	virtual ~InfoView() = default;
	InfoView(const InfoView& other) = default;
	InfoView(InfoView&& other) noexcept = default;
	InfoView& operator=(const InfoView& other) = default;
	InfoView& operator=(InfoView&& other) noexcept = default;
	virtual void showDataInfoFilm(std::shared_ptr<FilmBean> beanFilm) = 0;
};
