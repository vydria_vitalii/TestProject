﻿#pragma once
#include <QSqlQuery>
#include <memory>

template <class T>
class Extractor abstract
{
public:
	Extractor() = default;
	virtual ~Extractor() = default;
	virtual std::shared_ptr<T> extract(QSqlQuery& query) abstract;
	Extractor(const Extractor& other) = default;
	Extractor(Extractor&& other) noexcept = default;
	Extractor& operator=(const Extractor& other) = default;
	Extractor& operator=(Extractor&& other) noexcept = default;
};
