﻿#pragma once
#include "Presenter.h"


class InfoPresenter : public QObject, virtual public Presenter
{
	Q_OBJECT
		Q_INTERFACES(Presenter)

public:
	explicit InfoPresenter(QObject* const parent = nullptr);
	InfoPresenter(const InfoPresenter& other) = default;
	InfoPresenter(InfoPresenter&& other) = default;
	InfoPresenter& operator=(const InfoPresenter& other) = default;
	InfoPresenter& operator=(InfoPresenter&& other) = default;
	virtual void showSelectRecordFilm() = 0;

};

Q_DECLARE_INTERFACE(InfoPresenter, "Interface.InfoPresenter/1.0")
