﻿#pragma once
#include "Entity.h"
#include <vector> 
#include <memory>
#include "Position.h"
#include "Producer.h"
#include <QDateEdit>
#include <ostream>

using namespace std;

class Film : public Entity
{
public:
	explicit Film() : Entity(), idFilm(0), year(0), position()
	{
	};
	~Film() = default;
	Film(const Film& other);
	Film(Film&& other) noexcept;
	Film& operator=(const Film& other);
	Film& operator=(Film&& other) noexcept;
	friend bool operator==(const Film& lhs, const Film& rhs);
	friend bool operator!=(const Film& lhs, const Film& rhs);
	size_t hash() const;

	long long getIdFilm() const;
	void setIdFilm(const long long idFilm);
	string getTitleFilm();
	const string& getTitleFilm() const;
	template<class T>
	void setTitleFilm(T&& t);

	//void setTitleFilm(const string& titleFilm);
	unsigned long getYear() const;
	void setYear(const unsigned long year);
	Position getPosition() const;
	void setPostion(const Position &position);
	shared_ptr<vector<shared_ptr<Producer>>> getProducers();
	const shared_ptr<vector<shared_ptr<Producer>>>& getProducers() const;
	template<class T>void setProducers(T &&t);
	friend ostream& operator<<(std::ostream& os, const Film& obj);


	friend bool operator<(const Film& lhs, const Film& rhs);

	friend bool operator<=(const Film& lhs, const Film& rhs);

	friend bool operator>(const Film& lhs, const Film& rhs);

	friend bool operator>=(const Film& lhs, const Film& rhs);

private:
	long long idFilm;
	string titleFilm;
	unsigned long year;
	Position position;
	shared_ptr<vector<shared_ptr<Producer>>> producers;
};

template <class T>
void Film::setTitleFilm(T&& t)
{
	this->titleFilm = std::forward<T>(t);
}

template <class T>
void Film::setProducers(T &&t)
{
	this->producers = std::forward<T>(t);
}
