﻿#pragma once
#include "View.h"
#include "QDebug"
#include <QMainWindow>
#include "Film.h"
#include "FilmBean.h"

class MainView : virtual public View
{
	Q_INTERFACES(View)
public:
	virtual ~MainView() = default;
	virtual void showAddFilm() = 0;
	virtual void showFilms(vector<shared_ptr<Film>> films) = 0;
	virtual void enableInfoRecord(bool flag) = 0;
	virtual std::shared_ptr<FilmBean> getSelectRecordFilm() = 0;
	virtual void showInfoFilm() = 0;
	virtual bool isSearch() = 0;
	virtual shared_ptr<Film> getFilterData() = 0;

	//		film->setTitleFilm(ui->titleFilm->text().toStdString());
	//		film->setYear(ui->dateFilm->text().toLongLong());
	//		film->setPostion(static_cast<Position>(ui->postion->text().toInt()));
};
Q_DECLARE_INTERFACE(MainView, "Interface.MainView")