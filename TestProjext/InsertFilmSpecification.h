﻿#pragma once
#include <memory>
#include "Film.h"
#include "Specification.h"

class InsertFilmSpecification : public Specification
{
public:
	explicit InsertFilmSpecification(const std::shared_ptr<Film>& film);

	InsertFilmSpecification(const InsertFilmSpecification& other);
	InsertFilmSpecification(InsertFilmSpecification&& other) noexcept;
	InsertFilmSpecification& operator=(const InsertFilmSpecification& other);
	InsertFilmSpecification& operator=(InsertFilmSpecification&& other) noexcept;

	QMap<QString, QVector<QVariant>> specification() override;
private:
	std::shared_ptr<Film> film;
};
