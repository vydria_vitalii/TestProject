﻿#include "InsertFilmSpecification.h"

InsertFilmSpecification::InsertFilmSpecification(const std::shared_ptr<Film>& film) : film(film)
{
}

InsertFilmSpecification::InsertFilmSpecification(const InsertFilmSpecification& other) : film(other.film)
{
}

InsertFilmSpecification::InsertFilmSpecification(InsertFilmSpecification&& other) noexcept : film(std::move(other.film))
{
}

InsertFilmSpecification& InsertFilmSpecification::operator=(const InsertFilmSpecification& other)
{
	if (this == &other)
		return *this;
	film = other.film;
	return *this;
}

InsertFilmSpecification& InsertFilmSpecification::operator=(InsertFilmSpecification&& other) noexcept
{
	if (this == &other)
		return *this;
	film = std::move(other.film);
	return *this;
}

QMap<QString, QVector<QVariant>> InsertFilmSpecification::specification()
{
	QMap<QString, QVector<QVariant>> map;
	QVector<QVariant> data;
	QVariant variant;
	variant.setValue(QString::fromStdString(film->getTitleFilm()));
	data.push_back(move(variant));
	variant = QVariant();
	variant.setValue(film->getYear());
	data.push_back(move(variant));
	variant = QVariant();
	variant.setValue(static_cast<underlying_type<Position>::type>(film->getPosition()));
	data.push_back(move(variant));
	map.insert("INSERT INTO film(title_film, year, rating) VALUES (?, ?, ?);", move(data));

	for (auto& var : *film->getProducers())
	{
		variant = QVariant();
		data = QVector<QVariant>();
		variant.setValue(var->getIdProducer());
		data.push_back(variant);
		map.insert("INSERT INTO film_producer(producer_id, film_id) VALUES (?, ?)", move(data));
	}

	return map;
}
