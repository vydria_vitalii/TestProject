﻿#pragma once
#include "TransactionException.h"
#include <functional>
#include <forward_list>

class TransactionManager abstract
{
public:
	virtual ~TransactionManager() = default;
	template<class T>
	T execute(std::function<T()> &&operation)
		throw(TransactionException)
	{
		transaction();
		T res = operation();
		try
		{
			commit();
		}
		catch (TransactionException &ex)
		{
			roollback();
			throw TransactionException(ex);
		}
		return res;
	}

protected:
	virtual void transaction() = 0;
	virtual void commit() throw(TransactionException) = 0;
	virtual void roollback() = 0;
};
