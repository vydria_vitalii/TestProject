#pragma once
#include <string>

enum class Position : size_t
{
	NONE = 0,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHTN,
	NINE,
	TEN
};

inline std::ostream& operator << (std::ostream& os, const Position& obj)
{
	return os << std::to_string(static_cast<std::underlying_type<Position>::type>(obj));
}
