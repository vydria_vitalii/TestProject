#include "InfoFilmDialog.h"
#include "InfoPresenterImpl.h"

InfoFilmDialog::InfoFilmDialog(MainPresenter *const mainPresenter, QWidget *parent)
	: QDialog(parent), ui(new Ui::InfoFilmDialog())
{
	ui->setupUi(this);
	infoPresenter = new InfoPresenterImpl(mainPresenter, this);
}

InfoFilmDialog::~InfoFilmDialog()
{
	delete ui;
}

void InfoFilmDialog::showEvent(QShowEvent* event)
{
	infoPresenter->showSelectRecordFilm();
	QDialog::showEvent(event);
}

void InfoFilmDialog::init()
{
	infoPresenter->injectView(this);
}

void InfoFilmDialog::showDataInfoFilm(std::shared_ptr<FilmBean> beanFilm)
{
	auto nameFilmn = QString::fromStdString(beanFilm->getNameFilm());
	ui->nameFilm->setText(nameFilmn);
	auto producer = QString::fromStdString(beanFilm->getProducers());
	ui->producer->setText(producer);
	ui->rating->setText(QString::number(beanFilm->getReiting()));
	ui->year->setText(QString::number(beanFilm->getYear()));
}

QWidget* InfoFilmDialog::getWidgetWindow()
{
	return this;
}
