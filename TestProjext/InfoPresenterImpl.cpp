﻿#include "InfoPresenterImpl.h"

InfoPresenterImpl::InfoPresenterImpl(MainPresenter* const mainPresenter, QObject* const parent)
	: InfoPresenter(parent), mainPresenter(mainPresenter), infoView(nullptr)
{
}

InfoPresenterImpl::InfoPresenterImpl(const InfoPresenterImpl& other) : mainPresenter(other.mainPresenter),
infoView(other.infoView)
{
}

InfoPresenterImpl::InfoPresenterImpl(InfoPresenterImpl&& other) noexcept : mainPresenter(other.mainPresenter),
infoView(other.infoView)
{
	other.mainPresenter = nullptr;
	other.infoView = nullptr;
}

InfoPresenterImpl& InfoPresenterImpl::operator=(const InfoPresenterImpl& other)
{
	if (this == &other)
		return *this;
	mainPresenter = other.mainPresenter;
	infoView = other.infoView;
	return *this;
}

InfoPresenterImpl& InfoPresenterImpl::operator=(InfoPresenterImpl&& other) noexcept
{
	if (this == &other)
		return *this;
	mainPresenter = other.mainPresenter;
	infoView = other.infoView;
	other.mainPresenter = nullptr;
	other.infoView = nullptr;
	return *this;
}

void InfoPresenterImpl::injectView(View* view)
{
	infoView = dynamic_cast<InfoView*>(view);
}

void InfoPresenterImpl::showSelectRecordFilm()
{
	auto res = mainPresenter->getSelectRecordFilm();
	infoView->showDataInfoFilm(res);
}
