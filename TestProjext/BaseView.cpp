﻿#include "BaseView.h"

BaseView::BaseView() : initFlag(true)
{
}

void BaseView::showWindow()
{
	auto w = getWidgetWindow();
	if (!w->isActiveWindow())
	{
		if (initFlag) {
			init();
			initFlag = false;
		}
		w->show();
	}
}

void BaseView::enableWindow(const bool flag)
{
	auto w = getWidgetWindow();
	w->setEnabled(flag);
}

void BaseView::closeWindow()
{
	getWidgetWindow()->close();
}
