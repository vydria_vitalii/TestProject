﻿#include "TransactionException.h"

TransactionException::TransactionException()
{
}

TransactionException::TransactionException(char const* const _Message): exception(_Message)
{
}

TransactionException::TransactionException(char const* const _Message, const int i): exception(_Message, i)
{
}

TransactionException::TransactionException(exception const& _Other): exception(_Other)
{
}

TransactionException::TransactionException(const TransactionException& other): exception(other)
{
}

TransactionException::TransactionException(TransactionException&& other) noexcept: exception(std::move(other))
{
}

TransactionException& TransactionException::operator=(const TransactionException& other)
{
	if (this == &other)
		return *this;
	exception::operator =(other);
	return *this;
}

TransactionException& TransactionException::operator=(TransactionException&& other) noexcept
{
	if (this == &other)
		return *this;
	exception::operator =(std::move(other));
	return *this;
}
