﻿#include "finally.h"
#include <cstdlib>

//finally::finally(const std::function<void()>& fun) : functor(fun)
//{
//}//finally::finally(std::function<void()>& function) : function(function)
////{
////}
////
////finally::~finally()
////{
////	function();
////}
//finally::~finally()
//{
//	functor();
//}
using namespace finalizer;

FinallyHelper::FinallyHelper(std::function<void()>&& finalizer) : finalizer(std::move(finalizer))
{
}

FinallyHelper::~FinallyHelper()
{
	if (finalizer)
	{
		finalizer();
	}
}

