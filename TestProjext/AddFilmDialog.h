#pragma once

#include <QDialog>
#include "ui_AddFilmDialog.h"
#include "BaseView.h"
#include "AddFilmView.h"
#include "MainPresenter.h"
#include "str.h"
#include "ProducerService.h"
#include <QMessageBox>
#include "FilmService.h"

class AddFilmPresenter;

namespace Ui {
	class AddFilmDialog;
}

class AddFilmDialog : public QDialog, public AddFilmView, public BaseView
{
private:
	Q_OBJECT
		Q_INTERFACES(AddFilmView BaseView)
public:
	explicit AddFilmDialog(FilmService  *filmService, ProducerService *producerService, MainPresenter *mainPresenter, 
		QWidget *parent = Q_NULLPTR);
	~AddFilmDialog();
	std::shared_ptr<Film> getFilmData() override;
	void showProducer(std::vector<shared_ptr<Producer>> producers) override;
	void addProducer(shared_ptr<Producer> producer) override;
	void showError() override;
protected:
	void showEvent(QShowEvent*) override;

	void closeEvent(QCloseEvent*) override;
	void init() override;
	QWidget* getWidgetWindow() override;

private:
	static constexpr const size_t MIN_MAX_HEIGTH = 521;
	static constexpr const size_t MIN_MAX_WIDTH = 348;
	Ui::AddFilmDialog *ui;

	MainPresenter *mainPresenter;
	AddFilmPresenter *addFilmPresenter;
	ProducerService *producerService;
	FilmService  *filmService;
	private slots:
	void validationForm() const;
};

