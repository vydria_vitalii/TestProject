#pragma once

#include <QObject>
#include "Presenter.h"
#include "MainView.h"
#include "FilmBean.h"
class MainPresenter : public QObject, virtual public Presenter
{
	Q_OBJECT
		Q_INTERFACES(Presenter)
public:
	explicit MainPresenter(QObject* const parent = nullptr);
	virtual ~MainPresenter() = default;
	MainPresenter(const MainPresenter& other) = default;
	MainPresenter(MainPresenter&& other) = default;
	MainPresenter& operator=(const MainPresenter& other) = default;
	MainPresenter& operator=(MainPresenter&& other) = default;

	virtual void addFilm() = 0;
	virtual void updateTable() = 0;
	virtual void loadDataFilms() = 0;
	virtual void selectRecordFilm() = 0;
	virtual std::shared_ptr<FilmBean> getSelectRecordFilm() = 0;
	virtual void infoFilm() = 0;
	virtual void updateFilm(std::shared_ptr<Film>) = 0;
	virtual void selectByFilter() = 0;
};

Q_DECLARE_INTERFACE(MainPresenter, "Interface.MainPresenter/1.0")
