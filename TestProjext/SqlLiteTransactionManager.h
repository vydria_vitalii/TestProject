﻿#pragma once
#include <QSqlQuery>
#include "TransactionException.h"
#include "TransactionManager.h"
#include <QFile>
using namespace  std;

class SqlLiteTransactionManager : public TransactionManager
{
public:
	SqlLiteTransactionManager(const string&, const string&) throw(TransactionException);
	~SqlLiteTransactionManager();
	bool isTransaction() const;
protected:
	void connectToDataBase() throw(TransactionException);
	void openDataBase() throw(TransactionException);
	void createSkhemaTable(const string& query) throw(TransactionException);
	void restoreDataBase() throw(TransactionException);
	void closeDataBase();

	//transaction
	void transaction() override;
	void commit() throw(TransactionException) override;
	void roollback() override;

private:
	static const string SQL_FILM_TABLE;
	static const string SQL_FILM_PRODUCER_TABLE;
	static const string SQL_PRODUCER_TABLE;

	bool transactionDriver;
	const string dbName;
	const string dbHost;
	QSqlDatabase db;
};
