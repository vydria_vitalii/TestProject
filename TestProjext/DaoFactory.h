﻿#ifndef  DAO_FACTORY_H
#define  DAO_FACTORY_H

#include <memory>
#include "FilmDao.h"
#include "ProducerDao.h"
using namespace  std;

class DaoFactory abstract
{
public:
	static constexpr const int SQL_LITE = 1;
	DaoFactory() = default;
	virtual ~DaoFactory() {}
	DaoFactory(const DaoFactory& other) = default;
	DaoFactory(DaoFactory&& other) noexcept = default;
	DaoFactory& operator=(const DaoFactory& other) = default;
	DaoFactory& operator=(DaoFactory&& other) noexcept = default;

	virtual shared_ptr<FilmDao> getFimDao() const abstract;
	virtual shared_ptr<ProducerDao> getProducerDao() const abstract;


	static shared_ptr<DaoFactory> getDaoFactory(const int whichFactory) throw(invalid_argument);
};
#endif //DAO_FACTORY
