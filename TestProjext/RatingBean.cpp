﻿#include "RatingBean.h"

long long RatingBean::getIdFilm() const
{
	return idFilm;
}

void RatingBean::setIdFilm(const long long id_film)
{
	idFilm = id_film;
}

Position RatingBean::getPosition() const
{
	return position;
}

void RatingBean::setPosition(const Position position)
{
	this->position = position;
}
