﻿#pragma once
#include "Extractor.h"
#include "Film.h"
#include <QDebug>
#include <QSqlRecord>

class FilmExtractor : public Extractor<Film>
{
#define ID_FILM "id_film"
#define TITLE_FILM "title_film"
#define YEAR "year"
#define POSITION "rating"

public:
	explicit FilmExtractor(Extractor<Producer>* extractor);
	~FilmExtractor() = default;
	FilmExtractor(const FilmExtractor& other);
	FilmExtractor(FilmExtractor&& other) noexcept;
	FilmExtractor& operator=(const FilmExtractor& other);
	FilmExtractor& operator=(FilmExtractor&& other) noexcept;

	std::shared_ptr<Film> extract(QSqlQuery& query) override;

private:
	Extractor<Producer> *extractor;
};
