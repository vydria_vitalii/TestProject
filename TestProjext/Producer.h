﻿#pragma once
#include "Entity.h"
#include <functional>
#include <string>
#include <iostream>
using namespace std;

class Producer : public Entity
{
public:
	explicit  Producer() = default;
	~Producer() = default;


	Producer(const Producer& other)
		: Entity(other),
		idProducer(other.idProducer),
		nameProducer(other.nameProducer)
	{
	}

	Producer(Producer&& other) noexcept
		: Entity(std::move(other)),
		idProducer(other.idProducer),
		nameProducer(std::move(other.nameProducer))
	{
		idProducer = 0;
	}

	Producer& operator=(const Producer& other)
	{
		if (this == &other)
			return *this;
		Entity::operator =(other);
		idProducer = other.idProducer;
		nameProducer = other.nameProducer;
		return *this;
	}

	Producer& operator=(Producer&& other) noexcept
	{
		if (this == &other)
			return *this;
		Entity::operator =(std::move(other));
		idProducer = other.idProducer;
		other.idProducer = 0;
		nameProducer = std::move(other.nameProducer);
		return *this;
	}


	friend std::ostream& operator<<(std::ostream& os, const Producer& obj)
	{
		return os
			<< " idProducer: " << obj.idProducer
			<< " nameProducer: " << obj.nameProducer;
	}

	long long getIdProducer() const;
	void setIdProducer(const long long id_producer);
	std::string getNameProducer() const;
	//	void setNameProducer(const std::string name_producer);
	template<class T> void setNameProducer(T&& t);

	friend bool operator==(const Producer& lhs, const Producer& rhs);

	friend bool operator!=(const Producer& lhs, const Producer& rhs);

	size_t hash() const;

private:
	long long idProducer;
	string nameProducer;
};

template <class T>
void Producer::setNameProducer(T&& t)
{
	this->nameProducer = std::forward<T>(t);
}
