﻿#include "Film.h"


Film::Film(const Film& other) : Entity(other),
idFilm(other.idFilm),
titleFilm(other.titleFilm), year(0),
position(other.position),
producers(other.producers)
{
}

Film::Film(Film&& other) noexcept: Entity(move(other)),
idFilm(other.idFilm),
titleFilm(move(other.titleFilm)), year(0),
position(other.position),
producers(move(other.producers))
{
	other.position = Position::NONE;
}

Film& Film::operator=(const Film& other)
{
	if (this == &other)
		return *this;
	Entity::operator =(other);
	idFilm = other.idFilm;
	titleFilm = other.titleFilm;
	position = other.position;
	producers = other.producers;
	return *this;
}

Film& Film::operator=(Film&& other) noexcept
{
	if (this == &other) {
		return *this;
	}
	Entity::operator =(std::move(other));
	idFilm = other.idFilm;
	other.idFilm = 0;
	titleFilm = std::move(other.titleFilm);
	position = other.position;
	other.position = Position::NONE;
	producers = std::move(other.producers);
	return *this;
}


size_t Film::hash() const
{
	const size_t prime = 32;
	size_t result = 1;
	result += prime * result + std::hash<long long>()(idFilm);
	result += prime * result + std::hash<unsigned long>()(this->year);
	for (const auto &var : *this->producers)
	{
		result += prime * result + var->hash();
	}
	result += prime * result + std::hash<string>()(titleFilm);
	return result;
}

long long Film::getIdFilm() const
{
	return idFilm;
}

void Film::setIdFilm(const long long id_film)
{
	idFilm = id_film;
}

string Film::getTitleFilm()
{
	return this->titleFilm;
}

const string& Film::getTitleFilm() const
{
	return this->titleFilm;
}

unsigned long Film::getYear() const
{
	return year;
}

void Film::setYear(const unsigned long year)
{
	this->year = year;
}

Position Film::getPosition() const
{
	return position;
}

void Film::setPostion(const Position &position)
{
	this->position = position;
}

shared_ptr<vector<shared_ptr<Producer>>> Film::getProducers()
{
	return producers;
}


const shared_ptr<vector<shared_ptr<Producer>>>& Film::getProducers() const
{
	return this->producers;
}

bool operator==(const Film& lhs, const Film& rhs)
{
	return std::tie(lhs.idFilm, lhs.titleFilm, lhs.year, lhs.position, lhs.producers) == std::tie(rhs.idFilm, rhs.titleFilm, rhs.year, rhs.position, rhs.producers);
}

bool operator!=(const Film& lhs, const Film& rhs)
{
	return !(lhs == rhs);
}

ostream& operator<<(std::ostream& os, const Film& obj)
{
	return os << " idFilm: " << obj.idFilm
		<< " titleFilm: " << obj.titleFilm
		<< " year: " << obj.year
		<< " position: " << obj.position
		<< " producers: " << obj.producers;
}

bool operator<(const Film& lhs, const Film& rhs)
{
	return std::tie(lhs.idFilm, lhs.titleFilm, lhs.year, lhs.position, lhs.producers) < std::tie(rhs.idFilm, rhs.titleFilm, rhs.year, rhs.position, rhs.producers);
}

bool operator<=(const Film& lhs, const Film& rhs)
{
	return !(rhs < lhs);
}

bool operator>(const Film& lhs, const Film& rhs)
{
	return rhs < lhs;
}

bool operator>=(const Film& lhs, const Film& rhs)
{
	return !(lhs < rhs);
}
