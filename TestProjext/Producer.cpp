﻿#include "Producer.h"

long long Producer::getIdProducer() const
{
	return idProducer;
}

void Producer::setIdProducer(const long long id_producer)
{
	idProducer = id_producer;
}

std::string Producer::getNameProducer() const
{
	return nameProducer;
}

//void Producer::setNameProducer(const std::string name_producer)
//{
//	nameProducer = name_producer;
//}


bool operator==(const Producer& lhs, const Producer& rhs)
{
	return std::tie(lhs.idProducer, lhs.nameProducer) == std::tie(rhs.idProducer, rhs.nameProducer);
}

bool operator!=(const Producer& lhs, const Producer& rhs)
{
	return !(lhs == rhs);
}

size_t Producer::hash() const
{
	const size_t prime = 32;
	size_t result = 1;
	result += prime * result + std::hash<std::string>()(this->nameProducer);
	result += prime * result + std::hash<long long>()(idProducer);
	return result;
}
