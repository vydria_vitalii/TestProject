#include "MainWindow.h"
#include <QtWidgets/QApplication>

#include <QDebug>
#include <memory>
#include <QTextCodec>
#include "finally.h"
#include "MainWindowPresenterImpl.h"
#include "InfoFilmDialog.h"
#include "TransactionManager.h"
#include "SqlLiteTransactionManager.h"
#include "DaoFactory.h"
#include "FilmServiceImpl.h"

using namespace std;


struct A
{
	A() {
		qDebug() << QString("A\n");
	}
	A(const A& other)
		: a(other.a)
	{
		qDebug() << QString("copy constractro\n");
	}

	A(A&& other) noexcept
		: a(other.a)
	{
		qDebug() << QString("move constractro\n");
	}

	A& operator=(const A& other)
	{
		if (this == &other)
			return *this;
		a = other.a;
		qDebug() << QString("copy\n");

		return *this;
	}

	A& operator=(A&& other) noexcept
	{
		if (this == &other)
			return *this;
		a = other.a;
		qDebug() << QString("move\n");

		return *this;
	}
	~A() {
		qDebug() << QString("~A\n");
	}
	int *a = new int(6);
};

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	const string nameDb = "Resources\\sql\\films.db";
	const string host = "";

	TransactionManager *transaction = new SqlLiteTransactionManager(nameDb, host);
	auto dao = DaoFactory::getDaoFactory(DaoFactory::SQL_LITE);

	shared_ptr<View> v(new MainWindow(transaction, dao));
	v->showWindow();
	return a.exec();
}
