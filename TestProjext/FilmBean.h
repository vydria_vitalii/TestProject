﻿#pragma once
#include "Bean.h"
#include <ostream>
#include <filesystem>

class FilmBean : public Bean
{
public:
	FilmBean() = default;
	~FilmBean() = default;
	FilmBean(const FilmBean& other);

	FilmBean(FilmBean&& other) noexcept;

	FilmBean& operator=(const FilmBean& other);

	FilmBean& operator=(FilmBean&& other) noexcept;

	long long getIdFilm() const;
	void setIdFilm(const long long id_film);
	std::string getNameFilm() const;
	void setNameFilm(std::string name_film);
	std::string getProducers() const;
	void setProducers(std::string producers);
	size_t getReiting() const;
	void setReiting(const size_t reiting);
	long long getYear() const;
	void setYear(const long long year);

	friend bool operator==(const FilmBean& lhs, const FilmBean& rhs);
	friend bool operator!=(const FilmBean& lhs, const FilmBean& rhs);
	friend std::ostream& operator<<(std::ostream& os, const FilmBean& obj);
	friend std::size_t hash(const FilmBean& obj);

private:
	long long idFilm;
	std::string nameFilm;
	std::string producers;
	size_t reiting;
	long long year;
};
