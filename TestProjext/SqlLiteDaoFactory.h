﻿#ifndef SQL_LITE_DAO_FACTORY_H
#define SQL_LITE_DAO_FACTORY_H
#include "DaoFactory.h"
#include "SqlLiteFilmDao.h"

class SqlLiteDaoFactory : public DaoFactory
{
public:
	SqlLiteDaoFactory();
	~SqlLiteDaoFactory();


	SqlLiteDaoFactory(const SqlLiteDaoFactory& other);

	SqlLiteDaoFactory(SqlLiteDaoFactory&& other) noexcept;

	SqlLiteDaoFactory& operator=(const SqlLiteDaoFactory& other);

	SqlLiteDaoFactory& operator=(SqlLiteDaoFactory&& other) noexcept;

	shared_ptr<FilmDao> getFimDao() const override;
	shared_ptr<ProducerDao> getProducerDao() const override;
private:
	Extractor<Film> *filmExtractor;
	Extractor<Producer> *producerExtractor;
};

#endif//SQL_LITE_DAO_FACTORY_H