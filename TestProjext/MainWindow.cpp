#include "MainWindow.h"
#include <memory>
#include "AddFilmDialog.h"
#include "MainWindowPresenterImpl.h"
#include "AddFilmPresenter.h"
#include "AddFilmPresenterImpl.h"
#include "FilmServiceImpl.h"
#include "InfoFilmDialog.h"
#include "ChangeRatingDelegate.h"
#include "ProducerServiceImpl.h"


MainWindow::MainWindow(TransactionManager *transaction, std::shared_ptr<DaoFactory> &daoFactory, QWidget *parent)
	:QMainWindow(parent), ui(new Ui::MainWindow), addFilmDialog(nullptr),
	transaction(transaction), daoFactory(daoFactory), infoFilmView(nullptr), search(false)
{
	ui->setupUi(this);
	filmService = new FilmServiceImpl(transaction, daoFactory);
	produceService = new ProducerServiceImpl(transaction, daoFactory);
	mainPresenter = new MainWindowPresenterImpl(filmService, this);
}

void MainWindow::init()
{
	mainPresenter->injectView(this);
	connect(ui->addButton, &QPushButton::clicked, mainPresenter, &MainPresenter::addFilm);
	initTableModel();
	connect(ui->infoButton, &QPushButton::clicked, mainPresenter, &MainPresenter::infoFilm);

	connect(ui->searchButton, &QPushButton::clicked, [&]() mutable {
		search = true;
		mainPresenter->selectByFilter();
	});

	connect(ui->clearButton, &QPushButton::clicked, [&]() mutable {
		if (search) {
			search = false;
			mainPresenter->loadDataFilms();
		}
	});
}

MainWindow::~MainWindow()
{
	delete filmService;
	delete produceService;
	delete transaction;
	delete ui;
}

void MainWindow::showAddFilm()
{
	if (addFilmDialog == nullptr)
	{
		addFilmDialog = new AddFilmDialog(filmService, produceService, mainPresenter, this);
	}
	addFilmDialog->showWindow();
}

void MainWindow::showFilms(vector<shared_ptr<Film>> films)
{
	qDebug() << "showFilms strat";
	modelTable->removeRows(NULL, modelTable->rowCount());
	QList<QStandardItem*> rowData;
	for (auto &var : films)
	{
		rowData.clear();
		rowData << setItem(QString::number(var->getIdFilm()))
			<< setItem(QString::fromStdString(var->getTitleFilm()));
		QString str;
		for (auto &produsers : *(var->getProducers()))
		{
			str.append(QString("%1\t").arg(QString::fromStdString(produsers->getNameProducer())));
		}
		rowData << setItem(QString(str));
		rowData << setItem(QString::number(static_cast<underlying_type<Position>::type>(var->getPosition())), true);
		rowData << setItem(QString::number(var->getYear()));
		modelTable->appendRow(rowData);
	}
	qDebug() << "showFilms done";
}

void MainWindow::enableInfoRecord(bool flag)
{
	ui->infoButton->setEnabled(flag);
}

void MainWindow::showInfoFilm()
{
	qDebug() << "showInfoFilm strat";
	if (infoFilmView == nullptr)
	{
		infoFilmView = new InfoFilmDialog(mainPresenter, this);
	}
	infoFilmView->showWindow();
	qDebug() << "showInfoFilm done";

}

bool MainWindow::isSearch()
{
	return search;
}

shared_ptr<Film> MainWindow::getFilterData()
{
	shared_ptr<Film> film(new Film);
	film->setTitleFilm(ui->titleFilm->text().toStdString());
	film->setYear(ui->dateFilm->text().toLongLong());
	film->setPostion(static_cast<Position>(ui->postion->text().toInt()));
	return film;
}

QStandardItem* MainWindow::setItem(QString str, bool editable, Qt::Alignment alignment)
{
	QStandardItem *item = new QStandardItem(str);
	item->setEditable(editable);
	item->setTextAlignment(alignment);
	return item;
}

QWidget* MainWindow::getWidgetWindow()
{
	return this;
}

std::shared_ptr<FilmBean> MainWindow::getSelectRecordFilm()
{
	shared_ptr<FilmBean> bean(new FilmBean);
	auto select = ui->tableView->selectionModel();
	bean->setIdFilm(select->selectedRows(0).value(0).data().toLongLong());
	bean->setNameFilm(select->selectedRows(1).value(0).data().toString().toStdString());
	auto str = select->selectedRows(2).value(0).data().toString().replace("\t", ", ");
	str.remove(str.size() - 2, 2);
	bean->setProducers(str.toStdString());
	bean->setReiting(select->selectedRows(3).value(0).data().toInt());
	bean->setYear(select->selectedRows(4).value(0).data().toLongLong());
	return bean;
}

void MainWindow::initTableModel()
{
	modelTable = new QStandardItemModel(this);
	modelTable->setColumnCount(COLUMN_TABLE);
	const char *headerName[] = { FILM_HEADER_TABLE, PRODUCER_HEADER_TABLE, POSITION_HEDER_TABLE, YEAR_HEADER_TABLE };
	for (size_t i = 0, j = 0; i < COLUMN_TABLE || j < COLUMN_TABLE - 1; i++)
	{
		if (HIDEN_COLUMN_INDEX != i) {
			modelTable->setHorizontalHeaderItem(i, new QStandardItem(QString::fromLocal8Bit(headerName[j++])));
		}
		else
		{
			modelTable->setHorizontalHeaderItem(i, new QStandardItem("id"));
			ui->tableView->setColumnHidden(i, true);
		}
	}
	ui->tableView->setModel(modelTable);
	ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableView->horizontalHeader()->setSectionResizeMode(NULL, QHeaderView::Stretch);
	auto delegateEditor = new ChangeRatingDelegate(this);
	connect(delegateEditor, &ChangeRatingDelegate::updatetRating, mainPresenter, &MainPresenter::updateFilm);
	ui->tableView->setItemDelegateForColumn(3, delegateEditor);
	mainPresenter->loadDataFilms();
}
