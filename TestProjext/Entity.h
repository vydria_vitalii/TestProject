﻿#pragma once
#include <tuple>
#include <ostream>

class Entity abstract
{
public:
	Entity() = default;
	Entity(const Entity& other) = default;
	Entity(Entity&& other) = default;
	Entity& operator=(const Entity& other) = default;
	Entity& operator=(Entity&& other) noexcept = default;
	virtual ~Entity() = default;
	friend bool operator==(const Entity& lhs, const Entity& rhs);
	friend bool operator!=(const Entity& lhs, const Entity& rhs);
	friend std::size_t hash(const Entity& obj);
	friend std::ostream& operator<<(std::ostream& os, const Entity& obj);


};
