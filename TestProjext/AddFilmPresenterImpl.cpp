﻿#include "AddFilmPresenterImpl.h"


AddFilmPresenterImpl::AddFilmPresenterImpl(FilmService *filmService, ProducerService *service, MainPresenter *maintPresenter, QObject* const parent)
	: AddFilmPresenter(parent), mainPresenter(maintPresenter), addFilmView(nullptr), producerService(service), filmService(filmService)
{
}

void AddFilmPresenterImpl::injectView(View* view)
{
	qDebug() << "injectView strat";
	addFilmView = dynamic_cast<AddFilmView*>(view);
	qDebug() << "injectView done";
}

void AddFilmPresenterImpl::addFilm()
{
	qDebug() << "addFilm strat";
	auto film = addFilmView->getFilmData();
	if (filmService->addFilm(film))
	{
		mainPresenter->updateTable();
		addFilmView->closeWindow();
	}
	else
	{
		addFilmView->showError();
	}
	qDebug() << "addFilm done";
}

void AddFilmPresenterImpl::addProducer(std::shared_ptr<Producer> producer)
{
	qDebug() << "addProducer strat";
	auto res = producerService->addProducer(producer);
	addFilmView->addProducer(res);
	qDebug() << "addProducer done";
}

void AddFilmPresenterImpl::loadProducers()
{
	qDebug() << "loadProducers strat";
	auto prodicers = producerService->getAllProducers();
	addFilmView->showProducer(move(prodicers));
	qDebug() << "loadProducers done";
}
