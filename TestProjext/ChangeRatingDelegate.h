﻿#pragma once
#include <QItemDelegate>
#include <memory>
#include "Film.h"

class ChangeRatingDelegate : public QItemDelegate
{
	Q_OBJECT

		signals :
	void updatetRating(std::shared_ptr<Film> bean) const;
public:
	explicit ChangeRatingDelegate(QObject* const parent = nullptr);
	QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
	void setEditorData(QWidget* editor, const QModelIndex& index) const override;
	void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const override;
	void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

};
