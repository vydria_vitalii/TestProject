﻿#include "ProducerExtractor.h"

std::shared_ptr<Producer> ProducerExtractor::extract(QSqlQuery& query)
{
	qDebug() << "ProducerExtractor start";
	std::shared_ptr<Producer> producer(new Producer);
	auto&& rec = query.record();
	producer->setIdProducer(query.value(rec.indexOf(ID_PRODUCER)).toLongLong());
	producer->setNameProducer(query.value(rec.indexOf(NAME_PRODUCER)).toString().toStdString());
	qDebug() << "ProducerExtractor done";
	return producer;
}
