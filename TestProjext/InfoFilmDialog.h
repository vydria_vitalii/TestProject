#pragma once

#include <QDialog>
#include "ui_InfoFilmDialog.h"
#include "MainPresenter.h"
#include "InfoView.h"
#include "BaseView.h"
#include "InfoPresenter.h"

namespace Ui { class InfoFilmDialog; };

class InfoFilmDialog : public QDialog, public InfoView, public BaseView
{
	Q_OBJECT

public:
	explicit InfoFilmDialog(MainPresenter *const mainPresenter, QWidget *parent = Q_NULLPTR);
	~InfoFilmDialog();

protected:
	void showEvent(QShowEvent* event) override;
	void init() override;
public:
	void showDataInfoFilm(std::shared_ptr<FilmBean> beanFilm) override;
protected:
	QWidget* getWidgetWindow() override;
private:
	Ui::InfoFilmDialog *ui;
	InfoPresenter *infoPresenter;
};
