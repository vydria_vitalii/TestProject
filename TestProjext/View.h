﻿#ifndef VIEW_H
#define VIEW_H
#include "Initialize.h"
#include <qnamespace.h>
#include <QObject>

class View : virtual public Initialize
{
	Q_INTERFACES(Initialize)
public:
	View() = default;
	virtual ~View() = default;
	virtual void enableWindow(const bool flag) = 0;
	virtual void showWindow() = 0;
	virtual void closeWindow() = 0;
	//	virtual void setAttribute(Qt::WidgetAttribute, bool on = true) = 0;
};
Q_DECLARE_INTERFACE(View, "Interface.View")
#endif //VIEW_H
