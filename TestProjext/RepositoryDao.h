﻿#pragma once
#include <type_traits>
#include "Entity.h"
#include <vector>
#include "TransactionException.h"
#include <memory>

template <class T, bool = std::is_base_of<Entity, T>::value>
class RepositoryDao abstract
{
};

template <class T>
class RepositoryDao<T, true> abstract
{

public:
	RepositoryDao() = default;
	virtual ~RepositoryDao() = default;
	virtual std::shared_ptr<T> insert(std::shared_ptr<T>& obj) throw(TransactionException)abstract;
	virtual std::vector<std::shared_ptr<T>> findAll() throw(TransactionException)abstract;

	RepositoryDao(const RepositoryDao& other) = default;
	RepositoryDao(RepositoryDao&& other) noexcept = default;
	RepositoryDao& operator=(const RepositoryDao& other) = default;
	RepositoryDao& operator=(RepositoryDao&& other) noexcept = default;
};

