﻿#pragma once
#include "Specification.h"
#include "Producer.h"
#include <QVector>
#include <QDebug>

class InsertPresenterSpecification : public Specification
{
public:
	explicit InsertPresenterSpecification(const std::shared_ptr<Producer>& producer);
	~InsertPresenterSpecification();
	InsertPresenterSpecification(const InsertPresenterSpecification& other);
	InsertPresenterSpecification(InsertPresenterSpecification&& other) noexcept;
	InsertPresenterSpecification& operator=(const InsertPresenterSpecification& other);
	InsertPresenterSpecification& operator=(InsertPresenterSpecification&& other) noexcept;

	QMap<QString, QVector<QVariant>> specification() override;
private:
	std::shared_ptr<Producer> producer;
};
