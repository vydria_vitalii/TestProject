﻿#include "SqlLiteDaoFactory.h"
#include "SqlLiteFilmDao.h"
#include "ProducerExtractor.h"
#include "SqlLiteProducerDao.h"


SqlLiteDaoFactory::SqlLiteDaoFactory()
{
	producerExtractor = new ProducerExtractor();
	filmExtractor = new FilmExtractor(producerExtractor);
}

SqlLiteDaoFactory::~SqlLiteDaoFactory()
{
	if (producerExtractor != nullptr)
	{
		delete producerExtractor;
	}
	if (filmExtractor != nullptr)
	{
		delete filmExtractor;
	}
}

SqlLiteDaoFactory::SqlLiteDaoFactory(const SqlLiteDaoFactory& other) : SqlLiteDaoFactory()
{

}

SqlLiteDaoFactory::SqlLiteDaoFactory(SqlLiteDaoFactory&& other) noexcept : DaoFactory(std::move(other)),
filmExtractor(other.filmExtractor),
producerExtractor(other.producerExtractor)
{
	other.filmExtractor = nullptr;
	other.producerExtractor = nullptr;
}

SqlLiteDaoFactory& SqlLiteDaoFactory::operator=(const SqlLiteDaoFactory& other)
{
	if (this == &other)
		return *this;
	producerExtractor = new ProducerExtractor();
	filmExtractor = new FilmExtractor(producerExtractor);
	return *this;
}

SqlLiteDaoFactory& SqlLiteDaoFactory::operator=(SqlLiteDaoFactory&& other) noexcept
{
	if (this == &other)
		return *this;
	DaoFactory::operator =(std::move(other));
	filmExtractor = other.filmExtractor;
	producerExtractor = other.producerExtractor;
	other.filmExtractor = nullptr;
	other.producerExtractor = nullptr;
	return *this;
}

shared_ptr<FilmDao> SqlLiteDaoFactory::getFimDao() const
{
	static shared_ptr<FilmDao> filmDao(new SqlLiteFilmDao(filmExtractor));
	return  filmDao;
}

shared_ptr<ProducerDao> SqlLiteDaoFactory::getProducerDao() const
{
	static shared_ptr<ProducerDao> producerDao(new SqlLiteProducerDao(producerExtractor));
	return producerDao;
}
