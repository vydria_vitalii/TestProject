﻿#include "Bean.h"


std::ostream& operator<<(std::ostream& os, const Bean& obj)
{
	return os;
}

std::size_t hash(const Bean& obj)
{
	std::size_t seed = 0x2559D806;
	return seed;
}

bool operator==(const Bean& lhs, const Bean& rhs)
{
	return true;
}

bool operator!=(const Bean& lhs, const Bean& rhs)
{
	return !(lhs == rhs);
}

