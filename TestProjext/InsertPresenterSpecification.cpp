﻿#include "InsertPresenterSpecification.h"

InsertPresenterSpecification::InsertPresenterSpecification(const std::shared_ptr<Producer>& producer)
	: producer(producer)
{
}

InsertPresenterSpecification::InsertPresenterSpecification(const InsertPresenterSpecification& other)
	: Specification(other),
	producer(other.producer)
{
}

InsertPresenterSpecification::InsertPresenterSpecification(InsertPresenterSpecification&& other) noexcept
	: Specification(std::move(other)),
	producer(std::move(other.producer))
{
}

InsertPresenterSpecification& InsertPresenterSpecification::operator=(const InsertPresenterSpecification& other)
{
	if (this == &other)
		return *this;
	Specification::operator =(other);
	producer = other.producer;
	return *this;
}

InsertPresenterSpecification& InsertPresenterSpecification::operator=(InsertPresenterSpecification&& other) noexcept
{
	if (this == &other)
		return *this;
	Specification::operator =(std::move(other));
	producer = std::move(other.producer);
	return *this;
}

QMap<QString, QVector<QVariant>> InsertPresenterSpecification::specification()
{
	qDebug() << "InsertPresenterSpecification start";
	QMap<QString, QVector<QVariant>> speca;
	QVariant stringParam;
	stringParam.setValue(QString::fromStdString(producer->getNameProducer()));
	QVector<QVariant> v;
	v.push_back(move(stringParam));
	speca.insert("INSERT INTO producer(name_producer) VALUES(?)", move(v));
	qDebug() << "InsertPresenterSpecification done";
	return speca;
}

InsertPresenterSpecification::~InsertPresenterSpecification()
{
}
