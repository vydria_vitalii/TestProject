﻿#pragma once
#include <exception>

class TransactionException : public std::exception
{
public:
	TransactionException();

	explicit TransactionException(char const* const _Message);

	TransactionException(char const* const _Message, const int i);

	explicit TransactionException(exception const& _Other);

	TransactionException(const TransactionException& other);

	TransactionException(TransactionException&& other) noexcept;

	TransactionException& operator=(const TransactionException& other);

	TransactionException& operator=(TransactionException&& other) noexcept;
};
