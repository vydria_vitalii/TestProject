﻿#include "MainWindowPresenterImpl.h"

MainWindowPresenterImpl::MainWindowPresenterImpl(FilmService *filmService, QObject* const parent)
	: MainPresenter(parent), view(nullptr), filmService(filmService)
{
}

void MainWindowPresenterImpl::injectView(View* view)
{
	this->view = dynamic_cast<MainView*>(view);
}

void MainWindowPresenterImpl::addFilm()
{
	view->showAddFilm();
}

void MainWindowPresenterImpl::updateTable()
{
	if (view->isSearch())
	{
		selectByFilter();
	}
	else
	{
		loadDataFilms();
	}
}

void MainWindowPresenterImpl::selectRecordFilm()
{
	view->enableInfoRecord(true);
}

void MainWindowPresenterImpl::infoFilm()
{
	view->showInfoFilm();
}

void MainWindowPresenterImpl::selectByFilter()
{
	auto res = filmService->getFilmByFilter(view->getFilterData());
	view->showFilms(move(res));
}

void MainWindowPresenterImpl::updateFilm(std::shared_ptr<Film> bean)
{
	filmService->updateRating(bean);
}

void MainWindowPresenterImpl::loadDataFilms()
{
	auto films = filmService->getListFilms();
	view->showFilms(move(films));
}

std::shared_ptr<FilmBean> MainWindowPresenterImpl::getSelectRecordFilm()
{
	return view->getSelectRecordFilm();
}
