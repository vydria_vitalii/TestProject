﻿#pragma once
#include "AddFilmPresenter.h"
#include "AddFilmView.h"
#include "MainPresenter.h"
#include "ProducerService.h"
#include "FilmService.h"

class FilmService;

class AddFilmPresenterImpl : public AddFilmPresenter
{
	Q_OBJECT
		Q_INTERFACES(AddFilmPresenter)
public:
	explicit AddFilmPresenterImpl(FilmService * filmService, ProducerService *producerService,
		MainPresenter *maintPresenter, QObject* const parent = nullptr);
	void injectView(View* view) override;
	public slots:
	void addFilm() override;
	void addProducer(std::shared_ptr<Producer> producer) override;
	void loadProducers() override;
private:
	MainPresenter *mainPresenter;
	AddFilmView *addFilmView;
	ProducerService *producerService;
	FilmService *filmService;
};
