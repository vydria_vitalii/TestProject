﻿#pragma once
#include "RepositoryDao.h"
#include "Film.h"

class FilmDao abstract : virtual public RepositoryDao<Film>
{
public:
	FilmDao() = default;
	virtual ~FilmDao() = default;
	FilmDao(const FilmDao& other) = default;
	FilmDao(FilmDao&& other) noexcept = default;
	FilmDao& operator=(const FilmDao& other) = default;
	FilmDao& operator=(FilmDao&& other) noexcept = default;

	virtual bool updateRating(shared_ptr<Film> bean) throw(TransactionException)abstract;
	virtual shared_ptr<Film> findByName(QString name) throw(TransactionException)abstract;
	virtual vector<shared_ptr<Film>> findByFilter(shared_ptr<Film> film) throw(TransactionException)abstract;
};
