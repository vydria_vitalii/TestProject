﻿#include "ProducerServiceImpl.h"

ProducerServiceImpl::ProducerServiceImpl(TransactionManager* const transaction, shared_ptr<DaoFactory>& daoFactory) : transaction(transaction)
{
	producerDao = daoFactory->getProducerDao();
}

std::vector<std::shared_ptr<Producer>> ProducerServiceImpl::getAllProducers()
{
	qDebug() << "getAllProducers start";
	return transaction->execute<vector<shared_ptr<Producer>>>([&]() mutable
	{
		qDebug() << "getAllProducers done";
		return producerDao->findAll();
	});
}

shared_ptr<Producer> ProducerServiceImpl::addProducer(shared_ptr<Producer>& producer)
{
	qDebug() << "addProducer start";
	return  transaction->execute<shared_ptr<Producer>>([&]() mutable
	{
		qDebug() << "addProducer done";
		return producerDao->insert(producer);
	});
}
