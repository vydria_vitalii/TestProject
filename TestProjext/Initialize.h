﻿#pragma once

#include <QObject>

class Initialize 
{
public:
	virtual ~Initialize() = default;
protected:
	virtual void init() = 0;
};
Q_DECLARE_INTERFACE(Initialize, "Interface.Initialize")