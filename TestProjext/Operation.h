﻿#pragma once
#include <exception>

template<class  T>
class Operation abstract
{
public:
	virtual ~Operation() = default;
	virtual T execute() throw(std::exception)  abstract;
};
