﻿#pragma once
#include <vector>
#include <memory>
#include "Producer.h"

class ProducerService abstract
{
public:
	ProducerService() = default;
	virtual ~ProducerService() = default;

	ProducerService(const ProducerService& other) = default;
	ProducerService(ProducerService&& other) noexcept = default;
	ProducerService& operator=(const ProducerService& other) = default;
	ProducerService& operator=(ProducerService&& other) noexcept = default;


	virtual std::vector<std::shared_ptr<Producer>> getAllProducers() = 0;
	virtual shared_ptr<Producer> addProducer(shared_ptr<Producer>& producer) = 0;

};
