﻿#pragma once
#include <tuple>
#include <ostream>

class Bean abstract
{
public:
	Bean() = default;
	virtual ~Bean() = default;

	Bean(const Bean& other) = default;
	Bean(Bean&& other) noexcept = default;
	Bean& operator=(const Bean& other) = default;
	Bean& operator=(Bean&& other) noexcept = default;
	friend std::ostream& operator<<(std::ostream& os, const Bean& obj);
	friend std::size_t hash(const Bean& obj);
	friend bool operator==(const Bean& lhs, const Bean& rhs);
	friend bool operator!=(const Bean& lhs, const Bean& rhs);

};
