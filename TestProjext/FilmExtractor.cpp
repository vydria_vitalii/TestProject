﻿#include "FilmExtractor.h"
#include "ProducerExtractor.h"


FilmExtractor::FilmExtractor(Extractor<Producer>* extractor) : extractor(extractor)
{
}

FilmExtractor::FilmExtractor(const FilmExtractor& other) : Extractor<Film>(other),
extractor(other.extractor)
{
}

FilmExtractor::FilmExtractor(FilmExtractor&& other) noexcept : Extractor<Film>(std::move(other)),
extractor(other.extractor)
{
	//	other.extractor = nullptr;
}

FilmExtractor& FilmExtractor::operator=(const FilmExtractor& other)
{
	if (this == &other)
		return *this;
	Extractor<Film>::operator =(other);
	extractor = other.extractor;
	return *this;
}

FilmExtractor& FilmExtractor::operator=(FilmExtractor&& other) noexcept
{
	if (this == &other)
		return *this;
	Extractor<Film>::operator =(std::move(other));
	extractor = other.extractor;
	other.extractor = nullptr;
	return *this;
}

std::shared_ptr<Film> FilmExtractor::extract(QSqlQuery& query)
{
	qDebug() << "FilmExtractor start";
	std::shared_ptr<Film> film(new Film);
	auto&& rec = query.record();
	film->setIdFilm(query.value(rec.indexOf(ID_FILM)).toLongLong());
	film->setTitleFilm(query.value(rec.indexOf(TITLE_FILM)).toString().toStdString());
	size_t position = query.value(rec.indexOf(POSITION)).toInt();
	film->setPostion(static_cast<Position>(position));
	film->setYear(query.value(rec.indexOf(YEAR)).toLongLong());
	shared_ptr<vector<shared_ptr<Producer>>> producers(new vector<shared_ptr<Producer>>());
	auto producer = extractor->extract(query);
	producers->push_back(move(producer));
	film->setProducers(producers);
	qDebug() << "FilmExtractor done";
	return film;
}
