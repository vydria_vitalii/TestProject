﻿#pragma once
#include "Bean.h"
#include "Position.h"
#include <ostream>

class RatingBean : public Bean
{
public:
	RatingBean() = default;


	RatingBean(const RatingBean& other)
		: idFilm(other.idFilm),
		position(other.position)
	{
	}

	RatingBean(RatingBean&& other) noexcept
		: idFilm(other.idFilm),
		position(other.position)
	{
		other.idFilm = 0;
		other.position = Position::NONE;
	}

	RatingBean& operator=(const RatingBean& other)
	{
		if (this == &other)
			return *this;
		idFilm = other.idFilm;
		position = other.position;
		return *this;
	}

	RatingBean& operator=(RatingBean&& other) noexcept
	{
		if (this == &other)
			return *this;
		idFilm = other.idFilm;
		position = other.position;
		return *this;
	}


	friend std::ostream& operator<<(std::ostream& os, const RatingBean& obj)
	{
		return os
			<< "idFilm: " << obj.idFilm
			<< " position: " << obj.position;
	}


	friend std::size_t hash(const RatingBean& obj)
	{
		std::size_t seed = 0x05316B1E;
		seed ^= (seed << 6) + (seed >> 2) + 0x2BEA30C4 + static_cast<std::size_t>(obj.idFilm);
		seed ^= (seed << 6) + (seed >> 2) + 0x7024BFB8 + static_cast<std::size_t>(obj.position);
		return seed;
	}

	friend bool operator==(const RatingBean& lhs, const RatingBean& rhs)
	{
		return std::tie(lhs.idFilm, lhs.position) == std::tie(rhs.idFilm, rhs.position);
	}

	friend bool operator!=(const RatingBean& lhs, const RatingBean& rhs)
	{
		return !(lhs == rhs);
	}


	long long getIdFilm() const;
	void setIdFilm(const long long id_film);
	Position getPosition() const;
	void setPosition(const Position position);
private:
	long long idFilm;
	Position position;
};
