﻿#pragma once
#include <memory>
#include <vector>
#include <QVariant>
#include <QVector>

class Specification abstract
{
public:
	Specification() = default;
	virtual ~Specification() = default;

	Specification(const Specification& other) = default;
	Specification(Specification&& other) noexcept = default;
	Specification& operator=(const Specification& other) = default;
	Specification& operator=(Specification&& other) noexcept = default;

	virtual QMap<QString, QVector<QVariant>> specification() = 0;
};
