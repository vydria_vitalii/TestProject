﻿#pragma once
#include "View.h"
#include <QWidget>

class BaseView : virtual public View
{
	Q_INTERFACES(View)
public:
	BaseView();
	void showWindow() override;
	void enableWindow(const bool flag) override;
	void closeWindow() override;
protected:
	virtual QWidget* getWidgetWindow() = 0;
private:
	bool initFlag;
};

Q_DECLARE_INTERFACE(BaseView, "Interface.BaseView/1.0")
