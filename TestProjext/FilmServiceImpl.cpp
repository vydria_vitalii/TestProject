﻿#include "FilmServiceImpl.h"
#include <QDebug>

FilmServiceImpl::FilmServiceImpl(TransactionManager* const transaction, const shared_ptr<DaoFactory>& dao)
	: transaction(transaction)
{
	filmDao = dao->getFimDao();
}

FilmServiceImpl::FilmServiceImpl(const FilmServiceImpl& other) : transaction(other.transaction),
filmDao(other.filmDao)
{
}

FilmServiceImpl::FilmServiceImpl(FilmServiceImpl&& other) noexcept : transaction(other.transaction),
filmDao(std::move(other.filmDao))
{
	other.transaction = nullptr;
}

FilmServiceImpl& FilmServiceImpl::operator=(const FilmServiceImpl& other)
{
	if (this == &other)
		return *this;
	transaction = other.transaction;
	filmDao = other.filmDao;
	return *this;
}

FilmServiceImpl& FilmServiceImpl::operator=(FilmServiceImpl&& other) noexcept
{
	if (this == &other)
		return *this;
	transaction = other.transaction;
	other.transaction = nullptr;
	filmDao = std::move(other.filmDao);
	return *this;
}

vector<shared_ptr<Film>> FilmServiceImpl::getListFilms() throw(TransactionException)
{
	vector<shared_ptr<Film>> res = transaction->execute<vector<shared_ptr<Film>>>([&]() mutable {
		vector<shared_ptr<Film>> v = filmDao->findAll();
		return v;
	});
	return res;
}


vector<shared_ptr<Film>> FilmServiceImpl::getFilmByFilter(shared_ptr<Film> film) throw(TransactionException)
{
	return transaction->execute<vector<shared_ptr<Film>>>([&]() mutable
	{
		return filmDao->findByFilter(film);
	});
}

bool FilmServiceImpl::updateRating(shared_ptr<Film> bean) throw(TransactionException)
{
	return transaction->execute<bool>([&]() mutable {
		return filmDao->updateRating(bean);
	});
}


bool FilmServiceImpl::addFilm(shared_ptr<Film> film) throw(TransactionException)
{
	return transaction->execute<bool>([&]() mutable {
		auto res = filmDao->findByName(QString::fromStdString(film->getTitleFilm()));
		if (res == nullptr)
		{
			filmDao->insert(film);
			return true;
		}
		return false;
	});
}
