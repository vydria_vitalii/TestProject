﻿#pragma once
#include "FilmDao.h"
#include "AbstractSqlDao.h"
#include "FilmExtractor.h"
#include "Extractor.h"
#include <QDebug>
#include "InsertFilmSpecification.h"


class SqlLiteFilmDao : public FilmDao, public AbstractSqlDao<Film>
{
public:
	explicit SqlLiteFilmDao(Extractor<Film> *extractor);
	SqlLiteFilmDao(const SqlLiteFilmDao& other);

	SqlLiteFilmDao(SqlLiteFilmDao&& other) noexcept;
	SqlLiteFilmDao& operator=(const SqlLiteFilmDao& other);
	SqlLiteFilmDao& operator=(SqlLiteFilmDao&& other) noexcept;
	bool updateRating(shared_ptr<Film> bean) throw(TransactionException) override;
	std::vector<std::shared_ptr<Film>> findByFilter(std::shared_ptr<Film> film) throw(TransactionException) override;
	shared_ptr<Film> findByName(QString name) throw(TransactionException) override;

protected:
	void setGeneratingKey(std::shared_ptr<Film>& var, QSqlQuery& query) const override;
	shared_ptr<Specification> getInsertSpecification(shared_ptr<Film>& obj) const override;
	void convertRelationalModelToObject(vector<shared_ptr<Film>>& obj) const override;
	Extractor<Film> *&getExtractor() override;
	QString getQueryFindAll() const override;
private:
	Extractor<Film> *extractor;
};
