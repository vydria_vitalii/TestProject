﻿#pragma once
#include <vector>
#include <memory>
#include "Film.h"
#include "TransactionException.h"

using namespace std;
class FilmService abstract
{
public:
	virtual ~FilmService() = default;
	virtual vector<shared_ptr<Film>> getListFilms() throw(TransactionException)abstract;
	virtual bool updateRating(shared_ptr<Film> bean) throw(TransactionException)abstract;
	virtual  vector<shared_ptr<Film>> getFilmByFilter(shared_ptr<Film> film) throw(TransactionException)abstract;
	virtual bool addFilm(shared_ptr<Film> film) throw(TransactionException)abstract;
};

