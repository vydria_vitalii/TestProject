﻿#pragma once
#include "FilmService.h"
#include "DaoFactory.h"
#include "TransactionManager.h"
using namespace std;
class FilmServiceImpl : public FilmService
{
public:

	FilmServiceImpl(TransactionManager* const transaction, const shared_ptr<DaoFactory>& dao);
	FilmServiceImpl(const FilmServiceImpl& other);
	FilmServiceImpl(FilmServiceImpl&& other) noexcept;
	FilmServiceImpl& operator=(const FilmServiceImpl& other);
	FilmServiceImpl& operator=(FilmServiceImpl&& other) noexcept;
	vector<shared_ptr<Film>> getListFilms() throw(TransactionException) override;

	vector<shared_ptr<Film>> getFilmByFilter(shared_ptr<Film> film) throw(TransactionException) override;
	bool updateRating(shared_ptr<Film> bean) throw(TransactionException) override;

	bool addFilm(shared_ptr<Film> film) throw(TransactionException) override;
private:
	TransactionManager *transaction;
	shared_ptr<FilmDao> filmDao;
};
