#pragma once

#include "ui_MainWindow.h"
#include "MainView.h"
#include "BaseView.h"
#include <QStandardItemModel>
#include "AddFilmView.h"
#include "AddFilmDialog.h"
#include "MainPresenter.h"
#include "TransactionManager.h"
#include "DaoFactory.h"
#include "FilmService.h"
#include "InfoView.h"
#include "ProducerService.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow, public MainView, public BaseView
{
	Q_INTERFACES(MainView BaseView)
public:
	explicit MainWindow(TransactionManager *transaction, std::shared_ptr<DaoFactory> &daoFactory, QWidget *parent = Q_NULLPTR);
	~MainWindow();
	void showAddFilm() override;
	void showFilms(vector<shared_ptr<Film>> films) override;
	void enableInfoRecord(bool flag) override;
	void showInfoFilm() override;
	bool isSearch() override;
	shared_ptr<Film> getFilterData() override;
protected:
	void init() override;
	QWidget* getWidgetWindow() override;
	std::shared_ptr<FilmBean> getSelectRecordFilm() override;
private:
	static constexpr const int COLUMN_TABLE = 5;
	static constexpr const int HIDEN_COLUMN_INDEX = 0;
	static constexpr const char *FILM_HEADER_TABLE = "��������";
	static constexpr const char *PRODUCER_HEADER_TABLE = "��������";
	static constexpr const char *POSITION_HEDER_TABLE = "�������";
	static constexpr const char *YEAR_HEADER_TABLE = "���";

	Ui::MainWindow *ui;
	AddFilmDialog *addFilmDialog;
	MainPresenter *mainPresenter;
	TransactionManager *transaction;
	std::shared_ptr<DaoFactory> daoFactory;
	FilmService *filmService;
	QStandardItemModel *modelTable;
	InfoView *infoFilmView;
	bool search;
	ProducerService *produceService;

	void initTableModel();
	static QStandardItem* setItem(QString str, bool editable = false, Qt::Alignment alignment = Qt::AlignCenter);
};
