﻿#include "DaoFactory.h"
#include "SqlLiteDaoFactory.h"

shared_ptr<DaoFactory> DaoFactory::getDaoFactory(const int whichFactory) throw(invalid_argument)
{
	if (SQL_LITE == whichFactory)
	{
		shared_ptr<DaoFactory> dao(new SqlLiteDaoFactory);
		return dao;
	}
	throw invalid_argument("not found implementation: " + whichFactory);
}
