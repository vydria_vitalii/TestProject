﻿#pragma once
#include <QObject>
#include "Presenter.h"
#include <memory>
#include "Producer.h"

class AddFilmPresenter : public QObject, virtual public Presenter
{
	Q_OBJECT
		Q_INTERFACES(Presenter)
public:
	explicit AddFilmPresenter(QObject* const parent = nullptr);
	AddFilmPresenter(const AddFilmPresenter& other) = default;
	AddFilmPresenter(AddFilmPresenter&& other) = default;
	AddFilmPresenter& operator=(const AddFilmPresenter& other) = default;
	AddFilmPresenter& operator=(AddFilmPresenter&& other) = default;
	virtual ~AddFilmPresenter() = default;
	virtual void addProducer(std::shared_ptr<Producer> producer) = 0;
	virtual void loadProducers() = 0;
	virtual void addFilm() = 0;
};
Q_DECLARE_INTERFACE(AddFilmPresenter, "Interface.AddFilmPresenter/1.0")