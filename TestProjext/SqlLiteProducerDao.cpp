﻿#include "SqlLiteProducerDao.h"

SqlLiteProducerDao::SqlLiteProducerDao(Extractor<Producer>* const extractor) : extractor(extractor)
{
}

SqlLiteProducerDao::~SqlLiteProducerDao()
{
}

SqlLiteProducerDao::SqlLiteProducerDao(const SqlLiteProducerDao& other) : extractor(other.extractor)
{
}

SqlLiteProducerDao::SqlLiteProducerDao(SqlLiteProducerDao&& other) noexcept : extractor(other.extractor)
{
	other.extractor = nullptr;
}

SqlLiteProducerDao& SqlLiteProducerDao::operator=(const SqlLiteProducerDao& other)
{
	if (this == &other)
		return *this;
	extractor = other.extractor;
	return *this;
}

SqlLiteProducerDao& SqlLiteProducerDao::operator=(SqlLiteProducerDao&& other) noexcept
{
	if (this == &other)
		return *this;
	extractor = other.extractor;
	other.extractor = nullptr;
	return *this;
}

shared_ptr<Specification> SqlLiteProducerDao::getInsertSpecification(shared_ptr<Producer>& obj) const
{
	qDebug() << "getInsertSpecification start";
	shared_ptr<Specification> speca(new InsertPresenterSpecification(obj));
	qDebug() << "getInsertSpecification done";
	return speca;
}

void SqlLiteProducerDao::setGeneratingKey(std::shared_ptr<Producer>& var, QSqlQuery& query) const
{
	var->setIdProducer(query.lastInsertId().toLongLong());
}

QString SqlLiteProducerDao::getQueryFindAll() const
{
	return "SELECT * FROM producer;";
}

Extractor<Producer>*& SqlLiteProducerDao::getExtractor()
{
	return extractor;
}
