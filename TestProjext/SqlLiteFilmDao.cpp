﻿#include "SqlLiteFilmDao.h"
#include "ProducerExtractor.h"
#include "str.h"

SqlLiteFilmDao::SqlLiteFilmDao(Extractor<Film>* extractor) : extractor(extractor)
{
}

SqlLiteFilmDao::SqlLiteFilmDao(const SqlLiteFilmDao& other) : FilmDao(other),
AbstractSqlDao<Film, true>(other),
extractor(other.extractor)
{
}

SqlLiteFilmDao::SqlLiteFilmDao(SqlLiteFilmDao&& other) noexcept: FilmDao(std::move(other)),
AbstractSqlDao<Film, true>(std::move(other)),
extractor(std::move(other.extractor))
{
}

SqlLiteFilmDao& SqlLiteFilmDao::operator=(const SqlLiteFilmDao& other)
{
	if (this == &other)
		return *this;
	FilmDao::operator =(other);
	AbstractSqlDao<Film, true>::operator =(other);
	extractor = other.extractor;
	return *this;
}

SqlLiteFilmDao& SqlLiteFilmDao::operator=(SqlLiteFilmDao&& other) noexcept
{
	if (this == &other)
		return *this;
	FilmDao::operator =(std::move(other));
	AbstractSqlDao<Film, true>::operator =(std::move(other));
	extractor = std::move(other.extractor);
	return *this;
}

bool SqlLiteFilmDao::updateRating(shared_ptr<Film> bean) throw(TransactionException)
{
	qDebug() << "updateFilm start";
	QSqlQuery query;
	query.prepare("UPDATE film SET rating = ? WHERE id_film = ?;");
	query.addBindValue(static_cast<underlying_type<Position>::type>(bean->getPosition()));
	query.addBindValue(bean->getIdFilm());
	auto ret = query.exec();
	if (!ret)
	{
		qWarning() << "error updateFilm " << query.lastError().text();
	}
	qDebug() << "updateFilm done";
	return ret;
}

std::vector<std::shared_ptr<Film>> SqlLiteFilmDao::findByFilter(shared_ptr<Film> film) throw(TransactionException)
{
	qDebug() << "findByFilter start";
	auto name = film->getTitleFilm();
	trim(name);
	QString sql = "SELECT f.*, p.* FROM film as f INNER JOIN film_producer as f_p ON id_film = film_id INNER JOIN producer as p ON p.id_producer = f_p.producer_id WHERE ";

	if (!name.empty())
	{
		sql.append("f.title_film LIKE '%");
		transform(name.begin(), name.end(), name.begin(), ::toupper);
		sql.append(QString::fromStdString(name)).append("%'").append(" OR f.title_film LIKE '%");
		transform(name.begin(), name.end(), name.begin(), ::tolower);
		sql.append(QString::fromStdString(name)).append("%' OR ");
	}
	sql.append("f.year = ").append(QString::number(film->getYear())).append(" AND ");
	sql.append("f.rating = ").append(QString::number(static_cast<underlying_type<Position>::type>(film->getPosition())));
	qDebug() << "findByFilter done";
	return querySelect(sql);
}

shared_ptr<Film> SqlLiteFilmDao::findByName(QString name) throw(TransactionException)
{
	qDebug() << "fingByName start";
	QString query = "SELECT * FROM film WHERE REPLACE(UPPER(film.title_film), ' ','') =  REPLACE(UPPER('";
	query.append(name).append("'), ' ','')");
	auto result = querySelect(query);
	qDebug() << result.empty();
	if (result.empty())
	{
		return nullptr;
	}
	return  *result.begin();
}

void SqlLiteFilmDao::setGeneratingKey(std::shared_ptr<Film>& var, QSqlQuery& query) const
{
	qDebug() << "setGeneratingKey start";
	var->setIdFilm(query.lastInsertId().toLongLong());
	qDebug() << var->getIdFilm();
	qDebug() << "setGeneratingKey start";
}

shared_ptr<Specification> SqlLiteFilmDao::getInsertSpecification(shared_ptr<Film>& obj) const
{
	qDebug() << "getInsertSpecification start";
	shared_ptr<Specification> speca(new InsertFilmSpecification(obj));
	qDebug() << "getInsertSpecification done";
	return speca;
}

void SqlLiteFilmDao::convertRelationalModelToObject(vector<shared_ptr<Film>>& obj) const
{
	qDebug() << "convertRelationalModelToObject start";
	if (obj.size() > 0) {
		vector<shared_ptr<Film>> res;
		for (auto i = 0; i < obj.size() - 1; i++)
		{
			for (auto j = 1; j < obj.size(); j++)
			{
				if (obj[i]->getIdFilm() == obj[j]->getIdFilm() && obj[i] != obj[j])
				{
					auto src = obj[i]->getProducers();
					src->push_back(*obj[j]->getProducers()->begin());
					obj[j] = obj[i];
				}
			}
		}
		std::sort(obj.begin(), obj.end());
		obj.erase(unique(obj.begin(), obj.end()), obj.end());
	}
	qDebug() << "convertRelationalModelToObject done size " << obj.size();
}


Extractor<Film> *&SqlLiteFilmDao::getExtractor()
{
	return extractor;
}



QString SqlLiteFilmDao::getQueryFindAll() const
{
	return "SELECT f.*, p.* FROM film as f INNER JOIN film_producer as f_p ON id_film = film_id INNER JOIN producer as p ON p.id_producer = f_p.producer_id;";
}

