﻿#pragma once
#include "View.h"
#include <qobjectdefs.h>
#include <QObject>
#include <QDialog>
#include <memory>
#include "Film.h"

class AddFilmView : virtual public View
{
	Q_INTERFACES(View)
public:
	AddFilmView() = default;
	~AddFilmView() = default;

	virtual std::shared_ptr<Film> getFilmData() = 0;
	virtual void showProducer(std::vector<shared_ptr<Producer>>) = 0;
	virtual void addProducer(shared_ptr<Producer> producer) = 0;
	virtual void showError() = 0;

};
Q_DECLARE_INTERFACE(AddFilmView, "Interface.AddFilmView/1.0")