﻿#pragma once
#include "ProducerService.h"
#include "FilmDao.h"
#include "TransactionManager.h"
#include "ProducerDao.h"
#include "DaoFactory.h"
#include <QDebug>

class ProducerServiceImpl : public ProducerService
{
public:

	ProducerServiceImpl(TransactionManager* const transaction, shared_ptr<DaoFactory>& daoFactory);

	std::vector<std::shared_ptr<Producer>> getAllProducers() override;

	shared_ptr<Producer> addProducer(shared_ptr<Producer>& producer) override;
private:
	TransactionManager *transaction;
	shared_ptr<ProducerDao> producerDao;
};
