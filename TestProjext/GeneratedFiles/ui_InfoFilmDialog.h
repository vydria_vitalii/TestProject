/********************************************************************************
** Form generated from reading UI file 'InfoFilmDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFOFILMDIALOG_H
#define UI_INFOFILMDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_InfoFilmDialog
{
public:
    QLabel *label;
    QTextEdit *nameFilm;
    QLabel *label_2;
    QTextEdit *producer;
    QLabel *label_3;
    QTextEdit *rating;
    QLabel *label_4;
    QTextEdit *year;

    void setupUi(QDialog *InfoFilmDialog)
    {
        if (InfoFilmDialog->objectName().isEmpty())
            InfoFilmDialog->setObjectName(QStringLiteral("InfoFilmDialog"));
        InfoFilmDialog->setEnabled(true);
        InfoFilmDialog->resize(363, 308);
        label = new QLabel(InfoFilmDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setEnabled(true);
        label->setGeometry(QRect(20, 10, 141, 31));
        QFont font;
        font.setPointSize(11);
        label->setFont(font);
        nameFilm = new QTextEdit(InfoFilmDialog);
        nameFilm->setObjectName(QStringLiteral("nameFilm"));
        nameFilm->setGeometry(QRect(20, 50, 311, 71));
        nameFilm->setFont(font);
        nameFilm->setUndoRedoEnabled(false);
        nameFilm->setReadOnly(true);
        label_2 = new QLabel(InfoFilmDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setEnabled(true);
        label_2->setGeometry(QRect(20, 130, 141, 31));
        label_2->setFont(font);
        producer = new QTextEdit(InfoFilmDialog);
        producer->setObjectName(QStringLiteral("producer"));
        producer->setGeometry(QRect(20, 160, 311, 71));
        producer->setFont(font);
        producer->setUndoRedoEnabled(false);
        producer->setReadOnly(true);
        label_3 = new QLabel(InfoFilmDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setEnabled(true);
        label_3->setGeometry(QRect(20, 250, 71, 31));
        label_3->setFont(font);
        rating = new QTextEdit(InfoFilmDialog);
        rating->setObjectName(QStringLiteral("rating"));
        rating->setGeometry(QRect(80, 250, 91, 31));
        rating->setFont(font);
        rating->setUndoRedoEnabled(false);
        rating->setReadOnly(true);
        label_4 = new QLabel(InfoFilmDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setEnabled(true);
        label_4->setGeometry(QRect(190, 250, 41, 31));
        label_4->setFont(font);
        year = new QTextEdit(InfoFilmDialog);
        year->setObjectName(QStringLiteral("year"));
        year->setGeometry(QRect(230, 250, 101, 31));
        year->setFont(font);
        year->setUndoRedoEnabled(false);
        year->setReadOnly(true);

        retranslateUi(InfoFilmDialog);

        QMetaObject::connectSlotsByName(InfoFilmDialog);
    } // setupUi

    void retranslateUi(QDialog *InfoFilmDialog)
    {
        InfoFilmDialog->setWindowTitle(QApplication::translate("InfoFilmDialog", "\320\230\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217", Q_NULLPTR));
        label->setText(QApplication::translate("InfoFilmDialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265 \321\204\320\270\320\273\321\214\320\274\320\260", Q_NULLPTR));
        label_2->setText(QApplication::translate("InfoFilmDialog", "\320\240\320\265\320\266\320\270\321\201\321\201\320\265\321\200", Q_NULLPTR));
        label_3->setText(QApplication::translate("InfoFilmDialog", "\320\240\320\265\320\271\321\202\320\270\320\275\320\263", Q_NULLPTR));
        rating->setHtml(QApplication::translate("InfoFilmDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("InfoFilmDialog", "\320\223\320\276\320\264", Q_NULLPTR));
        year->setHtml(QApplication::translate("InfoFilmDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class InfoFilmDialog: public Ui_InfoFilmDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFOFILMDIALOG_H
