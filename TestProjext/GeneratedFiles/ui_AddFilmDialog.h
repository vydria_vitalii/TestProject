/********************************************************************************
** Form generated from reading UI file 'AddFilmDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDFILMDIALOG_H
#define UI_ADDFILMDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_AddFilmDialog
{
public:
    QTextEdit *nameFilm;
    QPushButton *addFilmButton;
    QLabel *label;
    QDateEdit *year;
    QLabel *label_2;
    QSpinBox *rating;
    QLabel *label_3;
    QLabel *label_4;
    QComboBox *listProducers;
    QLineEdit *newProducer;
    QLabel *label_5;
    QPushButton *addProducerButton;
    QPushButton *newProducerButton;
    QPushButton *removeProducerButton;
    QListWidget *producers;

    void setupUi(QDialog *AddFilmDialog)
    {
        if (AddFilmDialog->objectName().isEmpty())
            AddFilmDialog->setObjectName(QStringLiteral("AddFilmDialog"));
        AddFilmDialog->resize(348, 521);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AddFilmDialog->sizePolicy().hasHeightForWidth());
        AddFilmDialog->setSizePolicy(sizePolicy);
        AddFilmDialog->setSizeGripEnabled(false);
        AddFilmDialog->setModal(false);
        nameFilm = new QTextEdit(AddFilmDialog);
        nameFilm->setObjectName(QStringLiteral("nameFilm"));
        nameFilm->setGeometry(QRect(20, 50, 301, 61));
        QFont font;
        font.setPointSize(11);
        nameFilm->setFont(font);
        addFilmButton = new QPushButton(AddFilmDialog);
        addFilmButton->setObjectName(QStringLiteral("addFilmButton"));
        addFilmButton->setGeometry(QRect(150, 420, 161, 71));
        sizePolicy.setHeightForWidth(addFilmButton->sizePolicy().hasHeightForWidth());
        addFilmButton->setSizePolicy(sizePolicy);
        addFilmButton->setFont(font);
        label = new QLabel(AddFilmDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 30, 131, 16));
        label->setFont(font);
        year = new QDateEdit(AddFilmDialog);
        year->setObjectName(QStringLiteral("year"));
        year->setGeometry(QRect(20, 480, 71, 22));
        year->setFont(font);
        year->setCurrentSection(QDateTimeEdit::YearSection);
        year->setCalendarPopup(false);
        label_2 = new QLabel(AddFilmDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 460, 101, 16));
        label_2->setFont(font);
        rating = new QSpinBox(AddFilmDialog);
        rating->setObjectName(QStringLiteral("rating"));
        rating->setGeometry(QRect(20, 420, 61, 22));
        rating->setFont(font);
        rating->setMaximum(10);
        label_3 = new QLabel(AddFilmDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 400, 131, 16));
        label_3->setFont(font);
        label_4 = new QLabel(AddFilmDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 130, 131, 16));
        label_4->setFont(font);
        listProducers = new QComboBox(AddFilmDialog);
        listProducers->setObjectName(QStringLiteral("listProducers"));
        listProducers->setGeometry(QRect(20, 150, 181, 21));
        newProducer = new QLineEdit(AddFilmDialog);
        newProducer->setObjectName(QStringLiteral("newProducer"));
        newProducer->setGeometry(QRect(20, 210, 181, 21));
        newProducer->setFont(font);
        label_5 = new QLabel(AddFilmDialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 190, 131, 16));
        label_5->setFont(font);
        addProducerButton = new QPushButton(AddFilmDialog);
        addProducerButton->setObjectName(QStringLiteral("addProducerButton"));
        addProducerButton->setGeometry(QRect(230, 150, 91, 23));
        addProducerButton->setFont(font);
        newProducerButton = new QPushButton(AddFilmDialog);
        newProducerButton->setObjectName(QStringLiteral("newProducerButton"));
        newProducerButton->setGeometry(QRect(230, 210, 91, 23));
        newProducerButton->setFont(font);
        removeProducerButton = new QPushButton(AddFilmDialog);
        removeProducerButton->setObjectName(QStringLiteral("removeProducerButton"));
        removeProducerButton->setGeometry(QRect(230, 260, 91, 23));
        removeProducerButton->setFont(font);
        producers = new QListWidget(AddFilmDialog);
        producers->setObjectName(QStringLiteral("producers"));
        producers->setGeometry(QRect(20, 240, 181, 151));

        retranslateUi(AddFilmDialog);

        QMetaObject::connectSlotsByName(AddFilmDialog);
    } // setupUi

    void retranslateUi(QDialog *AddFilmDialog)
    {
        AddFilmDialog->setWindowTitle(QApplication::translate("AddFilmDialog", "\320\224\320\276\320\261\320\260\320\262\320\273\320\265\320\275\320\270\320\265 \321\204\320\270\320\273\321\214\320\274\320\260", Q_NULLPTR));
        addFilmButton->setText(QApplication::translate("AddFilmDialog", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", Q_NULLPTR));
        label->setText(QApplication::translate("AddFilmDialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265 \321\204\320\270\320\273\321\214\320\274\320\260", Q_NULLPTR));
        year->setDisplayFormat(QApplication::translate("AddFilmDialog", "yyyy", Q_NULLPTR));
        label_2->setText(QApplication::translate("AddFilmDialog", "\320\223\320\276\320\264", Q_NULLPTR));
        label_3->setText(QApplication::translate("AddFilmDialog", "\320\240\320\265\320\271\321\202\320\270\320\275\320\263", Q_NULLPTR));
        label_4->setText(QApplication::translate("AddFilmDialog", "\320\240\320\265\320\266\320\270\321\201\321\201\320\265\321\200", Q_NULLPTR));
        label_5->setText(QApplication::translate("AddFilmDialog", "\320\235\320\276\320\262\321\213\320\271 \321\200\320\265\320\266\320\270\321\201\321\201\320\265\321\200", Q_NULLPTR));
        addProducerButton->setText(QApplication::translate("AddFilmDialog", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", Q_NULLPTR));
        newProducerButton->setText(QApplication::translate("AddFilmDialog", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", Q_NULLPTR));
        removeProducerButton->setText(QApplication::translate("AddFilmDialog", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AddFilmDialog: public Ui_AddFilmDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDFILMDIALOG_H
