﻿#pragma once
#include "RepositoryDao.h"
#include <qlist.h>
#include <functional>
#include "Extractor.h"
#include "FilmExtractor.h"
#include <QSqlResult>
#include <QSqlError>
#include "Specification.h"

template <class T, bool = std::is_base_of<Entity, T>::value>
class AbstractSqlDao abstract
{
};

template<class T>
class AbstractSqlDao<T, true> abstract : virtual public RepositoryDao<T>
{
public:
	AbstractSqlDao() = default;
	~AbstractSqlDao() = default;
	AbstractSqlDao(const AbstractSqlDao& other) = default;
	AbstractSqlDao(AbstractSqlDao&& other) noexcept = default;
	AbstractSqlDao& operator=(const AbstractSqlDao& other) = default;
	AbstractSqlDao& operator=(AbstractSqlDao&& other) noexcept = default;

	std::shared_ptr<T> insert(shared_ptr<T>& obj) throw(TransactionException) override;
	vector<shared_ptr<T>> findAll() throw(TransactionException)  override;

protected:
	virtual QString getQueryFindAll() const;
	virtual shared_ptr<Specification> getInsertSpecification(shared_ptr<T> &) const = 0;
	virtual void setGeneratingKey(std::shared_ptr<T>& var, QSqlQuery &query) const = 0;
	virtual std::vector<shared_ptr<T>> querySelect(QString query);
	virtual void convertRelationalModelToObject(vector<shared_ptr<T>>& obj) const;
	virtual Extractor<T> *&getExtractor() abstract;
};


template <class T>
std::shared_ptr<T> AbstractSqlDao<T, true>::insert(shared_ptr<T>& obj) throw(TransactionException)
{
	qDebug() << "insert start";
	QSqlQuery query;
	std::shared_ptr<Specification> speca = getInsertSpecification(obj);
	auto i = 0;
	long long idKey = 0;
	auto &&mapSpeca = speca->specification();
	for (auto key : mapSpeca.keys())
	{
		if (i > 0)
		{
			idKey = query.lastInsertId().toLongLong();
		}
		query.prepare(key);
		auto params = mapSpeca.value(key);
		for (auto param : params)
		{
			query.addBindValue(param);
		}
		if (i++ > 0)
		{
			query.addBindValue(idKey);
		}
		if (!query.exec())
		{
			auto error = "error insert " + query.lastError().text().toStdString();
			qWarning() << QString::fromStdString(error);
			throw TransactionException(error.c_str());
		}
		if (idKey == 0)
		{
			setGeneratingKey(obj, query);
		}
	}
	qDebug() << "insert done";
	return obj;
}


template <class T>
std::vector<shared_ptr<T>> AbstractSqlDao<T, true>::findAll() throw(TransactionException)
{
	auto res = querySelect(getQueryFindAll());
	return res;
}


template <class T>
QString AbstractSqlDao<T, true>::getQueryFindAll() const
{
	throw exception("non implements getQueryInsert");
}


template <class T>
std::vector<shared_ptr<T>> AbstractSqlDao<T, true>::querySelect(QString query)
{
	qDebug() << "querySelect start";
	QSqlQuery querySql;
	qInfo() << "query " << query;

	if (!querySql.exec(query))
	{
		auto error = "error querySelect " + querySql.lastError().text().toStdString();
		qWarning() << QString::fromStdString(error);
		throw TransactionException(error.c_str());
	}
	std::vector<shared_ptr<T>> res;
	auto extractor = getExtractor();
	while (querySql.next())
	{
		res.push_back(extractor->extract(querySql));
	}
	convertRelationalModelToObject(res);
	qDebug() << "querySelect done";
	return res;
}

template <class T>
void AbstractSqlDao<T, true>::convertRelationalModelToObject(vector<shared_ptr<T>>& obj) const
{
	qDebug() << "convertRelationalModelToObject start";
	qDebug() << "convertRelationalModelToObject done";
}