﻿#pragma once
#include <qobjectdefs.h>
#include "InfoPresenter.h"
#include "InfoView.h"
#include "MainPresenter.h"

class InfoPresenterImpl : public InfoPresenter
{
	Q_OBJECT
		Q_INTERFACES(InfoPresenter)
public:

	explicit InfoPresenterImpl(MainPresenter*
		const mainPresenter, QObject* const parent = nullptr);
	InfoPresenterImpl(const InfoPresenterImpl& other);
	InfoPresenterImpl(InfoPresenterImpl&& other) noexcept;
	InfoPresenterImpl& operator=(const InfoPresenterImpl& other);
	InfoPresenterImpl& operator=(InfoPresenterImpl&& other) noexcept;

	void injectView(View* view) override;
	void showSelectRecordFilm() override;
private:
	MainPresenter *mainPresenter;
	InfoView *infoView;
};
