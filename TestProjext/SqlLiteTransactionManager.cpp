﻿#include "SqlLiteTransactionManager.h"
#include "TransactionException.h"
#include "finally.h"
#include <QSqlDriver>

const string SqlLiteTransactionManager::SQL_FILM_TABLE = "CREATE TABLE `film` (`id_film` INTEGER NOT NULL UNIQUE,`title_film` TEXT NOT NULL,`year` NUMERIC NOT NULL, `rating` INTEGER DEFAULT 0 CHECK(LENGTH(rating) >= 0), PRIMARY KEY(`id_film`));";
const string SqlLiteTransactionManager::SQL_FILM_PRODUCER_TABLE = "CREATE TABLE `film_producer` (`id_film_producer`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `film_id` INTEGER NOT NULL, `producer_id` INTEGER NOT NULL, FOREIGN KEY(`film_id`) REFERENCES `film`(`id_film`), FOREIGN KEY(`producer_id`) REFERENCES `producer`(`id_producer`));";
const string SqlLiteTransactionManager::SQL_PRODUCER_TABLE = "CREATE TABLE `producer` (`id_producer` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `name_producer`	TEXT NOT NULL UNIQUE); ";


SqlLiteTransactionManager::SqlLiteTransactionManager(const string& dbName, const string& dbHost) throw(TransactionException)
try :
	dbName(dbName), dbHost(dbHost)
{
	this->connectToDataBase();
}
catch (...)
{
	closeDataBase();
}

SqlLiteTransactionManager::~SqlLiteTransactionManager()
{
	closeDataBase();
}

bool SqlLiteTransactionManager::isTransaction() const
{
	return transactionDriver;
}

void SqlLiteTransactionManager::connectToDataBase() throw(TransactionException)
{
	if (!QFile(QString::fromStdString(dbName)).exists())
	{
		this->restoreDataBase();
	}
	else
	{
		openDataBase();
	}
}

void SqlLiteTransactionManager::openDataBase() throw(TransactionException)
{
	db = QSqlDatabase::addDatabase("QSQLITE");
	db.setHostName(QString::fromStdString(dbHost));
	db.setDatabaseName(QString::fromStdString(this->dbName));
	if (!db.open())
	{
		throw TransactionException("error open database");
	}
	this->transactionDriver = db.driver()->hasFeature(QSqlDriver::Transactions);
}

void SqlLiteTransactionManager::createSkhemaTable(const std::string& queryStr) throw(TransactionException)
{
	QSqlQuery query;
	const auto res = query.exec(QString::fromStdString(queryStr));
	if (!res)
	{
		throw TransactionException(string("error create table\sql: " + queryStr).c_str());
	}
}

void SqlLiteTransactionManager::restoreDataBase() throw(TransactionException)
{
	{
		auto file = new QFile(QString::fromStdString(this->dbName));
		file->open(QIODevice::WriteOnly);
		finally([&file]() mutable {
			file->close();
			delete file;
		});
	}

	openDataBase();
	const string *arr[] = { &SQL_FILM_TABLE, &SQL_PRODUCER_TABLE, &SQL_FILM_PRODUCER_TABLE };
	for (const auto &var : arr)
	{
		createSkhemaTable(*var);
	}
}

void SqlLiteTransactionManager::closeDataBase()
{
	if (db.isOpen() || db.isOpenError())
	{
		db.close();
	}
}


void SqlLiteTransactionManager::transaction()
{
#if transactionDriver
	db.transaction();
#endif
}

void SqlLiteTransactionManager::commit() throw(TransactionException)
{
#if transactionDriver
	if (!db.commit())
	{
		throw TransactionException("error commit");
	}
#endif
}

void SqlLiteTransactionManager::roollback()
{
#if transactionDriver
	db.roollback();
#endif
}
