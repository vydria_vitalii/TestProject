﻿#pragma once
#include <functional>

namespace finalizer {
	class FinallyHelper final
	{
	public:
		FinallyHelper() = delete;
		FinallyHelper(const FinallyHelper& other) = delete;
		explicit FinallyHelper(std::function<void()>&& finalizer);
		~FinallyHelper();

	private:
		std::function<void()> finalizer;
	};
}

#define finally(var) finalizer::FinallyHelper helper{var}

