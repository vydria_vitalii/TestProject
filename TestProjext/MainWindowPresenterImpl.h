﻿#pragma once
#include "MainPresenter.h"
#include "MainView.h"
#include <FilmService.h>

class MainWindowPresenterImpl : public MainPresenter
{
	Q_OBJECT
		Q_INTERFACES(MainPresenter)
public:
	explicit MainWindowPresenterImpl(FilmService *filmService, QObject* const parent = nullptr);
	void injectView(View* view) override;
	void loadDataFilms() override;
	std::shared_ptr<FilmBean> getSelectRecordFilm() override;
	public slots:
	void addFilm() override;
	void updateTable() override;
	void selectRecordFilm() override;
	void infoFilm() override;

	void selectByFilter() override;

	void updateFilm(std::shared_ptr<Film> bean) override;
private:
	MainView *view;
	FilmService *filmService;
};
