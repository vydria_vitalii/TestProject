﻿#include "FilmBean.h"

FilmBean::FilmBean(const FilmBean& other) : idFilm(other.idFilm),
nameFilm(other.nameFilm),
producers(other.producers),
reiting(other.reiting),
year(other.year)
{
}

FilmBean::FilmBean(FilmBean&& other) noexcept: idFilm(other.idFilm),
nameFilm(std::move(other.nameFilm)),
producers(std::move(other.producers)),
reiting(other.reiting),
year(other.year)
{
	other.idFilm = 0;
	other.reiting = 0;
	other.year = 0;
}

FilmBean& FilmBean::operator=(const FilmBean& other)
{
	if (this == &other)
		return *this;
	idFilm = other.idFilm;
	nameFilm = other.nameFilm;
	producers = other.producers;
	reiting = other.reiting;
	year = other.year;
	return *this;
}

FilmBean& FilmBean::operator=(FilmBean&& other) noexcept
{
	if (this == &other)
		return *this;
	idFilm = other.idFilm;
	nameFilm = std::move(other.nameFilm);
	producers = std::move(other.producers);
	reiting = other.reiting;
	year = other.year;

	other.idFilm = 0;
	other.reiting = 0;
	other.year = 0;
	return *this;
}

long long FilmBean::getIdFilm() const
{
	return idFilm;
}

void FilmBean::setIdFilm(const long long id_film)
{
	idFilm = id_film;
}

std::string FilmBean::getNameFilm() const
{
	return nameFilm;
}

void FilmBean::setNameFilm(std::string name_film)
{
	nameFilm = std::move(name_film);
}

std::string FilmBean::getProducers() const
{
	return producers;
}

void FilmBean::setProducers(std::string producers)
{
	this->producers = std::move(producers);
}

size_t FilmBean::getReiting() const
{
	return reiting;
}

void FilmBean::setReiting(const size_t reiting)
{
	this->reiting = reiting;
}

long long FilmBean::getYear() const
{
	return year;
}

void FilmBean::setYear(const long long year)
{
	this->year = year;
}

bool operator==(const FilmBean& lhs, const FilmBean& rhs)
{
	return std::tie(lhs.idFilm, lhs.nameFilm, lhs.producers, lhs.reiting, lhs.year) == std::tie(rhs.idFilm, rhs.nameFilm, rhs.producers, rhs.reiting, rhs.year);
}

bool operator!=(const FilmBean& lhs, const FilmBean& rhs)
{
	return !(lhs == rhs);
}

std::ostream& operator<<(std::ostream& os, const FilmBean& obj)
{
	return os
		<< "idFilm: " << obj.idFilm
		<< " nameFilm: " << obj.nameFilm
		<< " producers: " << obj.producers
		<< " reiting: " << obj.reiting
		<< " year: " << obj.year;
}

std::size_t hash(const FilmBean& obj)
{
	std::size_t seed = 0x4F9140C2;
	seed ^= (seed << 6) + (seed >> 2) + 0x2C5D9DB3 + static_cast<std::size_t>(obj.idFilm);
	seed ^= (seed << 6) + (seed >> 2) + 0x385C2DBE + std::experimental::filesystem::hash_value(obj.nameFilm);
	seed ^= (seed << 6) + (seed >> 2) + 0x2EA6DE56 + std::experimental::filesystem::hash_value(obj.producers);
	seed ^= (seed << 6) + (seed >> 2) + 0x6801A1D4 + static_cast<std::size_t>(obj.reiting);
	seed ^= (seed << 6) + (seed >> 2) + 0x0F87CD4C + static_cast<std::size_t>(obj.year);
	return seed;
}
