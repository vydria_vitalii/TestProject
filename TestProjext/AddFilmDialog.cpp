#include "AddFilmDialog.h"
#include "AddFilmPresenterImpl.h"


AddFilmDialog::AddFilmDialog(FilmService  *filmService, ProducerService *producerService, MainPresenter *mainPresenter, QWidget* parent)
	: QDialog(parent), ui(new Ui::AddFilmDialog), mainPresenter(mainPresenter), producerService(producerService)
{
	qDebug() << "AddFilmDialog start";
	ui->setupUi(this);
	QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	this->setSizePolicy(sizePolicy);
	this->setMinimumSize(QSize(MIN_MAX_WIDTH, MIN_MAX_HEIGTH));
	this->setMaximumSize(QSize(MIN_MAX_WIDTH, MIN_MAX_HEIGTH));
	this->setSizeGripEnabled(false);
	addFilmPresenter = new AddFilmPresenterImpl(filmService, producerService, mainPresenter, this);
	qDebug() << "AddFilmDialog done";

}

AddFilmDialog::~AddFilmDialog()
{
	qDebug() << "~AddFilmDialog start";
	delete ui;
	qDebug() << "~AddFilmDialog done";
}

std::shared_ptr<Film> AddFilmDialog::getFilmData()
{
	qDebug() << "getFilmData start";
	shared_ptr<Film> film(new Film);
	film->setTitleFilm(ui->nameFilm->toPlainText().trimmed().toStdString());
	film->setPostion(static_cast<Position>(ui->rating->value()));
	film->setYear(ui->year->text().toLongLong());
	shared_ptr<vector<shared_ptr<Producer>>> producers(new vector<shared_ptr<Producer>>);
	for (auto i = 0; i < ui->producers->count(); i++)
	{
		auto item = ui->producers->item(i);
		shared_ptr<Producer> producer(new Producer);
		producer->setIdProducer(item->data(Qt::UserRole).toLongLong());
		producer->setNameProducer(item->text().toLongLong());
		producers->push_back(producer);
	}
	film->setProducers(producers);
	qDebug() << "getFilmData done";
	return film;
}

void AddFilmDialog::showProducer(std::vector<shared_ptr<Producer>> producers)
{
	qDebug() << "showProducer start";
	for (auto& var : producers)
	{
		QVariant variant;
		variant.setValue(var->getIdProducer());
		ui->listProducers->addItem(QString::fromStdString(var->getNameProducer()), variant);
	}
	qDebug() << "showProducer done";
}

void AddFilmDialog::addProducer(shared_ptr<Producer> producer)
{
	qDebug() << "addProducer start";
	qDebug() << QString::fromStdString(producer->getNameProducer());
	QVariant variant;
	variant.setValue(producer->getIdProducer());
	ui->listProducers->addItem(QString::fromStdString(producer->getNameProducer()), variant);
	qDebug() << "addProducer done";
}

void AddFilmDialog::showError()
{
	QMessageBox::warning(this, QString::fromLocal8Bit("��������������"),
		QString::fromLocal8Bit("����� � ����� ������ ��� ����������").toUtf8());
}

void AddFilmDialog::showEvent(QShowEvent* event)
{
	qDebug() << "showEvent strat";
	QDialog::showEvent(event);
	ui->addFilmButton->setEnabled(false);
	ui->newProducerButton->setEnabled(false);
	addFilmPresenter->loadProducers();
	qDebug() << "showEvent done";
}

void AddFilmDialog::closeEvent(QCloseEvent*)
{
	qDebug() << "closeEvent start";
	ui->listProducers->clear();
	ui->newProducer->clear();
	ui->nameFilm->clear();
	ui->producers->clear();
	ui->year->clear();
	ui->rating->clear();
	qDebug() << "closeEvent done";
}

void AddFilmDialog::init()
{
	qDebug() << "init start";
	addFilmPresenter->injectView(this);
	connect(ui->nameFilm, &QTextEdit::textChanged, this, &AddFilmDialog::validationForm);
	connect(ui->addFilmButton, &QPushButton::clicked, addFilmPresenter, &AddFilmPresenter::addFilm);
	connect(ui->removeProducerButton, &QPushButton::clicked, [&]() mutable
	{
		auto item = ui->producers->currentItem();
		if (item != nullptr)
		{
			QVariant variant;
			variant.setValue(item->data(Qt::UserRole).toLongLong());
			ui->listProducers->addItem(item->text(), variant);
			ui->producers->takeItem(ui->producers->row(item));
			validationForm();
		}
	});
	connect(ui->addProducerButton, &QPushButton::clicked, [&]() mutable
	{
		QVariant variantText = ui->listProducers->currentText();
		if (!variantText.isNull()) {
			qDebug() << variantText;
			QVariant variantData = ui->listProducers->currentData();
			QListWidgetItem *item = new QListWidgetItem;
			item->setText(variantText.toString());
			item->setData(Qt::UserRole, variantData);
			ui->producers->insertItem(ui->producers->count(), item);
			ui->listProducers->removeItem(ui->listProducers->findData(variantData));
			validationForm();
		}
	});

	connect(ui->newProducerButton, &QPushButton::clicked, [&]() mutable
	{
		shared_ptr<Producer> producer(new Producer);
		producer->setNameProducer(ui->newProducer->text().toStdString());
		ui->newProducer->clear();
		addFilmPresenter->addProducer(std::move(producer));
	});

	connect(ui->newProducer, &QLineEdit::textChanged, [&]() mutable
	{
		QRegExp regex("\\s+");
		auto str = ui->newProducer->text().trimmed().remove(regex).toUpper();
		auto res = !str.isEmpty();
		for (auto i = 0; i < ui->listProducers->count() && res; i++)
		{
			auto item = ui->listProducers->itemText(i);
			qDebug() << item << str;
			if (item.remove(regex).toUpper().compare(str) == 0)
			{
				res = false;
				break;
			}
		}
		ui->newProducerButton->setEnabled(res);
	});
	qDebug() << "init done";
}

QWidget* AddFilmDialog::getWidgetWindow()
{
	qDebug() << "getWidgetWindow start/done";
	return this;
}

void AddFilmDialog::validationForm() const
{
	qDebug() << "validationForm start";
	auto ret = true;
	auto name = ui->nameFilm->toPlainText();
	name = name.trimmed();
	if (name.isEmpty() || ui->producers->count() == 0)
	{
		ret = false;
	}
	ui->addFilmButton->setEnabled(ret);
	qDebug() << "validationForm done";
}
