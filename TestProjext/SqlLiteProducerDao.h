﻿#pragma once
#include "ProducerDao.h"
#include "AbstractSqlDao.h"
#include "InsertPresenterSpecification.h"
#include <QSqlQuery>

class SqlLiteProducerDao : public ProducerDao, public AbstractSqlDao<Producer>
{
public:

	explicit SqlLiteProducerDao(Extractor<Producer>*const extractor);
	~SqlLiteProducerDao();

	SqlLiteProducerDao(const SqlLiteProducerDao& other);

	SqlLiteProducerDao(SqlLiteProducerDao&& other) noexcept;

	SqlLiteProducerDao& operator=(const SqlLiteProducerDao& other);

	SqlLiteProducerDao& operator=(SqlLiteProducerDao&& other) noexcept;

protected:

	shared_ptr<Specification> getInsertSpecification(shared_ptr<Producer>& obj) const override;

	void setGeneratingKey(std::shared_ptr<Producer>& var, QSqlQuery& query) const override;

	QString getQueryFindAll() const override;

	Extractor<Producer>*& getExtractor() override;
private:
	Extractor<Producer> *extractor;
};
