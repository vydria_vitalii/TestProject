﻿#pragma once
#include "RepositoryDao.h"
#include "Producer.h"

class ProducerDao abstract : virtual public RepositoryDao<Producer>
{
public:
	ProducerDao() = default;
	virtual ~ProducerDao() = default;

};
