﻿#include "ChangeRatingDelegate.h"
#include <QDebug>
#include <memory>
#include <QSpinBox>

ChangeRatingDelegate::ChangeRatingDelegate(QObject* const parent) : QItemDelegate(parent)
{
}

QWidget* ChangeRatingDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	auto spinBoxEditor = new QSpinBox(parent);
	spinBoxEditor->setMinimum(static_cast<std::underlying_type<Position>::type>(Position::NONE));
	spinBoxEditor->setMaximum(static_cast<std::underlying_type<Position>::type>(Position::TEN));
	spinBoxEditor->setAlignment(Qt::AlignCenter);
	return spinBoxEditor;
}

void ChangeRatingDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	auto value = index.model()->data(index, Qt::EditRole).toInt();
	auto spinBox = static_cast<QSpinBox*>(editor);
	spinBox->setValue(value);
}

void ChangeRatingDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
	auto spinBox = static_cast<QSpinBox*>(editor);
	spinBox->interpretText();
	auto value = spinBox->value();
	QVariant prev = model->data(index);
	if (prev.toInt() != value) {
		model->setData(index, value, Qt::EditRole);
		auto id = model->data(model->index(index.row(), NULL), Qt::DisplayRole);
		std::shared_ptr<Film> bean(new Film);
		bean->setIdFilm(id.toLongLong());
		bean->setPostion(static_cast<Position>(value));
		emit updatetRating(bean);
	}
}

void ChangeRatingDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	editor->setGeometry(option.rect);
}
