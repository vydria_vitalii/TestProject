﻿#pragma once
#include "View.h"
#include <QObject>

class Presenter abstract
{
public:
	Presenter() = default;
	virtual ~Presenter() = default;

	virtual void injectView(View *view) = 0;
	Presenter(const Presenter& other) = default;
	Presenter(Presenter&& other) noexcept = default;
	Presenter& operator=(const Presenter& other) = default;
	Presenter& operator=(Presenter&& other) noexcept = default;
	
};
Q_DECLARE_INTERFACE(Presenter, "Interface.Presenter")