﻿#pragma once
#include "Extractor.h"
#include "Producer.h"
#include <QSqlRecord>
#include "QSqlQuery.h"
#include <QVariant>
#include <QDebug>

class ProducerExtractor : public Extractor<Producer>
{
#define ID_PRODUCER "id_producer"
#define NAME_PRODUCER "name_producer"

public:
	ProducerExtractor() = default;
	~ProducerExtractor() = default;

	ProducerExtractor(const ProducerExtractor& other) = default;
	ProducerExtractor(ProducerExtractor&& other) noexcept = default;
	ProducerExtractor& operator=(const ProducerExtractor& other) = default;
	ProducerExtractor& operator=(ProducerExtractor&& other) noexcept = default;
	std::shared_ptr<Producer> extract(QSqlQuery& query) override;
};
